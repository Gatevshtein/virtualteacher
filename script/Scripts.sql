CREATE DATABASE  IF NOT EXISTS `virtual_teacher_DB` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `virtual_teacher_DB`;
-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: virtual_teacher_DB
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `authorities` (
  `authority_id` int(11) NOT NULL AUTO_INCREMENT,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`authority_id`),
  UNIQUE KEY `authority` (`authority`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (2,'ROLE_ADMIN'),(3,'ROLE_STUDENT'),(1,'ROLE_TEACHER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `category_image` varchar(100) NOT NULL DEFAULT 'pic.jpg',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Development','dev.jpg'),(2,'Business','business.jpg'),(3,'Finance & Accounting','finance.jpg'),(4,'IT & Software','it.jpg'),(5,'Office Productivity','office.jpg'),(6,'Psychology','psych.jpg'),(7,'Design','design.jpg'),(8,'Marketing','marketing.jpg'),(9,'Lifestyle','lifestyle.jpg'),(10,'Photography','photography.jpg'),(11,'Health & Fitness','yoga.jpg'),(12,'Music','music.jpg'),(13,'Teaching & Academics','teaching.jpg');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_ratings`
--

DROP TABLE IF EXISTS `course_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course_ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `rate_by_student` int(11) NOT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `completed_completed_courses` (`course_id`),
  KEY `users_completed_courses` (`user_id`),
  CONSTRAINT `completed_completed_courses` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `users_completed_courses` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_ratings`
--

LOCK TABLES `course_ratings` WRITE;
/*!40000 ALTER TABLE `course_ratings` DISABLE KEYS */;
INSERT INTO `course_ratings` VALUES (7,25,16,16),(9,50,27,91),(10,60,24,41),(11,61,21,100),(12,60,27,0),(13,50,28,90),(14,62,24,0);
/*!40000 ALTER TABLE `course_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_description` text NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isSubmitted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `title` (`title`),
  KEY `courses_categories` (`category_id`),
  KEY `courses_users` (`author_id`),
  CONSTRAINT `courses_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `courses_users` FOREIGN KEY (`author_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (11,'The Joy of Painting',25,7,'Bob Ross and the Joy of Painting is enjoyed by happy painters all around the world. Certified Ross Instructors in the landscape, floral and wildlife techniques can be found from north and south America to Asia, the middle east and Europe. Use this directory to find a class near you.',0,1),(12,'Yin Yoga',27,11,'Our Yin Yoga Flow class is going to give you that relaxing deep stretch your body has been craving for.',0,1),(14,'Introduction to film photography',29,10,'A series of bite-sized, 60-second animations from ILFORD Photo to introduce the terminology and basic techniques used in film photography and darkroom printing.',0,0),(15,'A Walk in the Woods',25,7,'ima li efect tova',1,1),(16,'Ecology',44,13,'From balancing chemical reactions to analyzing famous literature, never before has one collection offered such an awe-inspiring range of content. Since 2011, brothers John and Hank Green have pioneered digital education with their series Crash Course on YouTube.\r\n\r\nFrom Science to Social Studies, Crash Course offers it all in quick-paced, imaginative videos aimed at learners of all ages. Explore the amazing collection here, with new resources and courses added constantly to broaden the reaches of the Crash Course experience.',0,1),(17,'Yoga Outdoors',27,11,'Enjoy beautiful landskapes',0,0),(21,'Eastern European Men School',51,9,'The best Eastern European video on Nerdy Penguin hands down',0,1),(22,'Finance for Dummies',52,3,'Finance is a field that is concerned with the allocation (investment) of assets and liabilities over space and time, often under conditions of risk or uncertainty. Finance can also be defined as the art of money management.',0,1),(23,'Big Data',53,8,'Big Data -- yet another buzz word. But what is it? This video has big data explained in laymen terms. Big Data is simply our ability to take massive amounts of data and derive a narrative from it.   Not too long ago, performing analytics on such massive amounts of data was infeasible. The popularity of distributed computing and parallel computing allowed for \"Big Data\". In technical terms, we only scratch the surface. But consider this video: Big Data Simplified with Big Data Examples.  Big Data Marketing is also picking up. In this video we show you how major companies like Walmart, Netflix & Facebook use Big Data in their marketing & sales efforts to drastically increase their profits & presence. Small companies & startups are also taking advantage of big data analytics & big data companies to target their customers inexpensively.  We hope you enjoy this video on Big Data Marketing :)  We\'d love to hear your thoughts -- write us a comment!',0,0),(24,'Ukulele',55,12,'Welcome to my guitar lessons channel aimed at beginners! Here you will find structured guitar tutorials & easy songs all ideal for beginners!  All my videos are embedded on my website http://www.andyguitar.co.uk/ so head over there especially if you want to see the structure to the songs & lessons I\'ve created!',0,1),(25,'Algorithms',56,4,'n mathematics and computer science, an algorithm is a set of instructions, typically to solve a class of problems or perform a computation. Algorithms are unambiguous specifications for performing calculation, data processing, automated reasoning, and other tasks.',0,0),(26,'The Science of Productivity',57,5,'In today\'s crazy world, productivity is on the minds of many. So what can science tell us about the human brain and productive work? How do we become more efficient at working, and spend less time working overall?   Written and created by Mitchell Moffit (twitter @mitchellmoffit) and Gregory Brown (twitter @whalewatchmeplz).',0,1),(27,'Understand Dreaming',58,6,'Dreams are hallucinations that occur during certain stages of sleep. They’re strongest during REM sleep, or the rapid eye movement stage, when you may be less likely to recall your dream. Much is known about the role of sleep in regulating our metabolism, blood pressure, brain function, and other aspects of health. But it’s been harder for researchers to explain the role of dreams.  When you’re awake, your thoughts have a certain logic to them. When you sleep, your brain is still active, but your thoughts or dreams often make little or no sense. This may be because the emotional centers of the brain trigger dreams, rather than the logical regions.  Though there’s no definitive proof, dreams are usually autobiographical thoughts based on your recent activities, conversations, or other issues in your life. However, there are some popular theories on the role of dreams.',0,1),(28,'Brain Tricks',57,6,'Ever wonder how your brain processes information? These brain tricks and illusions help to demonstrate the two main systems of Fast and Slow Thinking in your brain.   Written and created by Mitchell Moffit and Gregory Brown',0,1),(29,'How to Make Lecture Videos',59,13,'Learn about how to produce videos with the CGM lab at FL Poly.',0,1),(30,'A Camping Blog Series',64,9,'The basic steps for setting up any modern pitched tent. Watch closely as Justin demonstrates how to quickly set up a tent solo. Please be sure to subscribe to my channel for other fun and informative videos.',0,0),(31,'How to Ride a Bike',55,11,'Very important skill',1,0),(32,'Be happy don\'t worry',77,9,'Most important things in summer',0,0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolled_courses`
--

DROP TABLE IF EXISTS `enrolled_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `enrolled_courses` (
  `enrolled_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`enrolled_id`),
  KEY `enrolled_courses_courses` (`course_id`),
  KEY `enrolled_courses_users` (`user_id`),
  CONSTRAINT `enrolled_courses_courses` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `enrolled_courses_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolled_courses`
--

LOCK TABLES `enrolled_courses` WRITE;
/*!40000 ALTER TABLE `enrolled_courses` DISABLE KEYS */;
INSERT INTO `enrolled_courses` VALUES (14,27,11),(21,29,12),(25,50,21),(29,62,16),(33,27,29),(34,27,24),(35,50,24),(36,65,27),(37,67,12),(38,68,21),(39,69,22),(40,71,27),(41,72,24),(42,55,16),(43,74,24),(44,76,24),(45,77,21);
/*!40000 ALTER TABLE `enrolled_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homeworks`
--

DROP TABLE IF EXISTS `homeworks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `homeworks` (
  `homework_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL,
  `is_graded` tinyint(1) NOT NULL DEFAULT '0',
  `file_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`homework_id`),
  KEY `homeworks_lessons` (`lesson_id`),
  KEY `homeworks_users` (`student_id`),
  CONSTRAINT `homeworks_lessons` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`lesson_id`),
  CONSTRAINT `homeworks_users` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homeworks`
--

LOCK TABLES `homeworks` WRITE;
/*!40000 ALTER TABLE `homeworks` DISABLE KEYS */;
INSERT INTO `homeworks` VALUES (25,27,10,89,1,'Juliana10.jpg'),(26,25,17,50,1,'BobRoss17.jpg'),(27,25,18,80,1,'BobRoss18.jpg'),(28,27,11,79,1,'Juliana11.jpg'),(33,29,12,0,0,'christoph12312.jpg'),(38,50,28,77,1,'pikachu28.gif'),(39,50,43,78,1,'pikachu43.jpg'),(40,60,36,67,1,'MileKitich36.jpg'),(41,60,37,81,1,'MileKitich37.jpg'),(42,61,28,80,1,'ShakerMaker28.jpg'),(43,61,29,65,1,'ShakerMaker29.jpg'),(44,61,30,100,1,'ShakerMaker30.png'),(45,62,17,0,0,'Naruto17.jpg'),(46,62,36,60,1,'Naruto36.jpg'),(47,62,37,100,1,'Naruto37.jpeg'),(48,60,43,79,1,'MileKitich43.jpg'),(49,50,44,82,1,'pikachu44.jpg'),(51,50,36,90,1,'pikachu36.jpg'),(52,67,12,0,0,'bombateacher12.jpg'),(53,68,28,0,0,'MegaDaskal28.jpg'),(54,69,31,0,0,'djakuzitamakuzita31.jpg'),(55,71,43,0,0,'Hackerche43.jpg'),(56,72,36,80,1,'Shef4e36.jpg'),(57,74,36,90,1,'NovTeacher36.jpg'),(58,76,36,88,1,'MadMax36.jpg'),(59,77,28,80,1,'KakaZlatka28.jpeg');
/*!40000 ALTER TABLE `homeworks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'imageID1.jpg'),(2,'imageID2.jpg'),(3,'imageID3.jpg'),(6,'Negative0-15-14(1).jpg'),(12,'ImageOfNerdyPenguinAdmin24.jpg'),(14,'ImageOfpikachu50.png'),(15,'ImageOfJuliana27.jpg'),(16,'ImageOfBobRoss25.jpg'),(17,'ImageOfNerdyPenguinAdmin24.jpg'),(18,'ImageOfSquattingSlav51.jpg'),(19,'ImageOfKurzgesagt52.png'),(20,'ImageOfCrashCourse44.jpg'),(21,'ImageOfAndyGuitar55.jpg'),(22,'ImageOfHackerRank56.jpg'),(23,'ImageOfHackerRank56.jpg'),(24,'ImageOfAsapSCIENCE57.png'),(25,'ImageOfHypnos58.jpg'),(26,'ImageOfMileKitich60.jpg'),(27,'ImageOfShakerMaker61.jpeg'),(28,'ImageOfNaruto62.jpg'),(29,'ImageOfNaruto62.jpg'),(30,'ImageOfNaruto62.png'),(31,'ImageOfShakerMaker61.jpg'),(32,'ImageOfShakerMaker61.jpg'),(33,'ImageOfAndyGuitar55.jpg'),(34,'ImageOfNerdyPenguinAdmin24.jpg'),(35,'ImageOfKakaZlatka77.jpg');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lessons` (
  `lesson_id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_title` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `lesson_description` text NOT NULL,
  `homework_file_name` varchar(100) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lesson_id`),
  KEY `lessons_courses` (`course_id`),
  KEY `lessons_videos` (`video_id`),
  CONSTRAINT `lessons_courses` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `lessons_videos` FOREIGN KEY (`video_id`) REFERENCES `videos` (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` VALUES (10,'A Walk in the Woods',11,17,'Bob Ross introduces us to his \"Almighty\" assortment of tools and colors, tells us that anyone can paint, and creates a landscape of a forest path just after a rain shower.','PaintingHomework1.jpg',0),(11,'Mt. McKinley',11,18,'Thought today we\'d do a picture  that\'s right straight out of Alaska.  We\'ll do one of Mount McKinley,  the most almighty mountain in the North American continent.','PaintingHomework2.txt',0),(12,'Best Yoga For Flexibility',12,19,'Focusing on fusing Vinyasa based flow (Yang) with slow and tension releasing postures (Yin), this class is great to do in morning as well as anytime during the day as we focus on first warming up the body with a flow, and then taking it into a deep release by holding each Yin pose for 2 minutes. ','YogaHomework1.txt',0),(13,'Best Yoga For Slow Deep Muscle Release',12,20,'Re-energize your nervous system, let go of stress and tension, and become fully present in each moment of each posture as you let go of the past with each exhale you make.','YogaHomework2.jpg',0),(14,'How a Film SLR Camera Works',14,21,'A short 6o-second animation, showing how a film SLR camera works  Episode 1 of 11 in our \'How to\' series.  These short 60 second videos are aimed at those who are new to film photography as well as those looking to gain additional knowledge.','photography-lection01.jpg',0),(15,'Introduction to Film Formats',14,22,'A 60-second animation introducing the film formats offered by ILFORD PHOTO  Episode 2 of 11 in our \'How to\' series.  These short 60 second videos are aimed at those who are new to film photography as well as those looking to gain additional knowledge.  It covers a brief description on loading 35mm film, 120 roll film and large format sheet film.','photography-lection02.jpeg',0),(16,'Film ISO - A beginners guide by ILFORD Photo',14,24,'This video explains the speed ratings (ISO) of our films and its sensitivity, giving examples of which film to use in different light conditions.   These short 60 second videos are aimed at those who are new to film photography as well as those looking to gain additional knowledge.','photography-lection03.jpg',0),(17,'The History of Life on Earth',16,25,'With a solid understanding of biology on the small scale under our belts, it\'s time for the long view - for the next twelve weeks, we\'ll be learning how the living things that we\'ve studied interact with and influence each other and their environments. Life is powerful, and in order to understand how living systems work, you first have to understand how they originated, developed and diversified over the past 4.5 billion years of Earth\'s history. Hang on to your hats as Hank tells us the epic drama that is the history of life on Earth.','EcologyHomework01.jpeg',0),(18,'Population Ecology: The Texas Mosquito Mystery',16,26,'Population ecology is the study of groups within a species that interact mostly with each other, and it examines how they live together in one geographic area to understand why these populations are different in one time and place than they are in another.  How is that in any way useful to anyone ever? Hank uses the example a of West Nile virus outbreak in Texas to show you in this episode of Crash Course: Ecology. ','EcologyHomework02.pdf',0),(19,'Ebony Sunset',11,27,'Today, we’ll be working with black canvas. Bob Ross DOES give you instructions about what exactly he’s talking about as far as this goes – but only does so once you’re already well into the episode. I got advice from my Art Supply Friend (thanks, Friend!) who got me to just buy a black prepared canvas – which was about the same price as a regular prepared canvas.','Homework_Ebony Sunset11.jpg',0),(20,'Winter Mist',11,28,'\"I prefer winter and fall, when you feel the bone structure of the landscape — the loneliness of it; the dead feeling of winter. Something waits beneath it, the whole story doesn\'t show.\"  - Andrew Wyeth','Homework_Winter Mist11.jpg',0),(21,'Quiet Stream',11,29,'You can see the different brush strokes used and learn how to apply them, remember your first painting is more of a feel for the brushes and tools used to create. If you take the fan brush and make more of the waterfall look throughout the stream and lift up the stoke for the water breaks i think you\'ll find a smoother look. (the strokes used for the water breaks \"downward\" would be a fantastic tree illusion). second pointer would be to use less paint, and remember to blend your background. keep at it and you\'ll see improvement every time.','Homework_Quiet Stream11.jpg',0),(26,'Human Population Growth',16,35,'If being alive on Earth were a contest, humans would win it hands down.  We\'re like the Michael Phelps of being alive, but with 250,000 times more gold medals. Today Hank is here to tell us the specifics of why and how human population growth has happened over the past hundred and fifty years or so, and how those specifics relate to ecology.','Homework_Human Population Growth16.pdf',0),(27,'Feel the Love',16,36,'Interactions between species are what define ecological communities, and community ecology studies these interactions anywhere they take place.  Although interspecies interactions are mostly competitive, competition is pretty dangerous, so a lot of interactions are actually about side-stepping direct competition and instead finding ways to divvy up resources to let species get along.  Feel the love?','Homework_Feel the Love16.jpg',0),(28,'True Eastern European Wisdom',21,37,'Not just a lesson, but a lifestyle','Homework_True Eastern European Wisdom21.jpg',0),(29,'What is Gopnik',21,38,'Boris explains what is gopnik. With very many visual details and historic storyline. Very good for slav.','Homework_What is Gopnik21.png',0),(30,'Dancing Like a Slav',21,39,'crazy russian dancing in saint petersburg','Homework_Dancing Like a Slav21.html',0),(31,'Banking Explained – Money and Credit',22,40,'Banks are a riddle wrapped up in an enigma. We all kind of know that they do stuff with money we don’t understand, while the last crisis left a feeling of deep mistrust and confusion. We try to shed a bit of light onto the banking system. Why were banks invented, why did they cause the last crisis and are there alternatives?','Homework_Banking Explained – Money and Credit22.png',0),(32,'How The Stock Exchange Works',22,41,'Why are there stocks at all?  Everyday in the news we hear about the stock exchange, stocks and money moving around the globe. Still, a lot of people don\'t have an idea why we have stock markets at all, because the topic is usually very dry. We made a short video about the basics of the stock exchanges. With robots. Robots are kewl!  Short videos, explaining things. For example Evolution, the Universe, the Stock Market or controversial topics like Fracking. Because we love science.  We would love to interact more with you, our viewers to figure out what topics you want to see. If you have a suggestion for future videos or feedback, drop us a line! :)  We\'re a bunch of Information designers from munich, visit us on facebook or behance to say hi!','Homework_How The Stock Exchange Works22.pdf',0),(33,'Universal Basic Income Explained',22,42,'A universal basic income is a government guarantee that each citizen receives a minimum income. It is also called a citizen\'s income, guaranteed minimum income, or basic income. The intention behind the payment is to provide enough to cover the basic cost of living and provide financial security.','Homework_Universal Basic Income Explained22.zip',0),(34,'Big Data Marketing',23,43,'Big data refers to the ever-increasing volume, velocity, variety, variability and complexity of information. For marketing organizations, big data is the fundamental consequence of the new marketing landscape, born from the digital world we now live in.','Homework_Big Data Marketing23.png',0),(35,'Community Ecology II: Predators',16,44,'Hank gets to the more violent part of community ecology by describing predation and the many ways prey organisms have developed to avoid it.','Homework_Community Ecology II: Predators16.jpg',0),(36,'Absolute Beginner',24,45,'This course presumes no prior knowledge of Ukulele. The video begins with choosing an Ukulele and getting your Uke in tune etc.','Homework_Absolute Beginner24.jpg',0),(37,'Learning a Ukulele Song',24,46,'In this lesson, we\'re wasting no time and learning a modern Ukulele classic song; Rip Tide by Vance Joy!  This song uses just three chords for most of the song, which most people will have no problem changing between. It is the strumming that will be more challenging for many people. Follow my guidance in the video and you should be fine. If you struggle, try more of the suggested songs at the end of my day 1 video to get your confidence up, then come back to this to give it another shot!','Homework_Learning a Ukulele Song24.jpeg',0),(38,'Recursion',25,47,'Learn the basics of recursion. This video is a part of HackerRank\'s Cracking The Coding Interview Tutorial with Gayle Laakmann McDowell.','Homework_Recursion25.zip',0),(39,'Solve \'Ice Cream Parlor\' Using Binary Search',25,48,'In computer science, binary search, also known as half-interval search, logarithmic search, or binary chop, is a search algorithm that finds the position of a target value within a sorted array. Binary search compares the target value to the middle element of the array.','Homework_Solve \'Ice Cream Parlor\' Using Binary Search25.pdf',0),(40,'Quicksort',25,49,'Quicksort is an efficient sorting algorithm, serving as a systematic method for placing the elements of a random access file or an array in order. Developed by British computer scientist Tony Hoare in 1959 and published in 1961, it is still a commonly used algorithm for sorting.','Homework_Quicksort25.txt',0),(41,'The Science of Productivity',26,50,'There are only so many hours in the day, so making the most of your time is critical. There are two ways increase your output--either put in more hours or work smarter. I don\'t know about you, but I prefer the latter.  Being more productive at work isn\'t rocket science, but it does require being more deliberate about how you manage your time.','Homework_The Science of Productivity26.pdf',0),(42,'Office Etiquette',26,51,'Why is it that office etiquette is so important?  The basic answer is that with good office etiquette everyone can be comfortable and effective in the workplace. Of course, 100% comfort and efficiency is an impossible ideal but we can say that 90 percent is a good margin to shoot for and by using good etiquette techniques and behaviors, we think you can achieve 90% or perhaps even above, as far as etiquette is concerned.  What makes for good office etiquette then? Simply put, having good office etiquette means that you are respectful and considerate both of your co-workers and of the overall office environment around you.   With that said, here are a few situations where we think you should be especially mindful of having good workplace etiquette.','Homework_Office Etiquette26.png',0),(43,'Why do we dream',27,52,'In the 3rd millennium BCE, Mesopotamian kings recorded and interpreted their dreams on wax tablets. In the years since, we haven\'t paused in our quest to understand why we dream. And while we still don’t have any definitive answers, we have some theories. Amy Adkins reveals the top seven reasons why we might dream.  Lesson by Amy Adkins, animation by Clamanne Studio.','Homework_Why do we dream27.pdf',0),(44,'This Is How Your Brain Works',28,53,'In Thinking, Fast and Slow, Kahneman wrestles with flawed ideas about decision making. “Social scientists in the 1970s broadly accepted two ideas about human nature. First, people are generally rational…Second, emotions…explain most of the occasions on which people depart from rationality.” But research has “traced [systematic] errors to the… machinery of cognition…rather than corruption…by emotion.”','Homework_This Is How Your Brain Works28.jpg',0),(45,'How to Make Lecture Videos',29,54,'6 Tips for Creating Engaging Video Lectures That Students Will Actually Watch Author:  Simuelle Myers, Instructional Designer, TLC Image for Creating Engaging Video Lectures That Students Will Actually Watch How do I get started creating video lectures? How do I engage students from a distance? How do I know if they understand the concepts in my lecture if I can’t see their faces? How do I know students are even paying attention? These are common questions asked by faculty when preparing to create a video lecture. As both online and flipped classroom formats grow in popularity, the number of faculty creating video lectures is increasing. However, many of these videos are recorded as straight lecture, limiting students only to the role of observer.  The six tips below will help you create videos that also engage students in active learning, while giving you information to assess their understanding of course concepts.','Homework_How to Make Lecture Videos29.txt',0),(46,'How to set up a tent: 5 Easy Steps',30,55,'We\'ve all been there: It\'s getting dark, it\'s cold, a wind\'s coming up, and you\'ve got to sleep outside tonight. Pretty much the worst time ever to forget the tent instructions. Before you hike into the woods, you better know how to set that tent up by heart to avoid awkward and time-consuming efforts at your camp site. Learning to find the right spot on which to pitch your tent, how to put it together, and how to care for your tent, will make camping a much more enjoyable experience.','Homework_How to set up a tent: 5 Easy Steps30.jpg',0);
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending_for_approval`
--

DROP TABLE IF EXISTS `pending_for_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pending_for_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pending_for_approval_users` (`teacher_id`),
  CONSTRAINT `pending_for_approval_users` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending_for_approval`
--

LOCK TABLES `pending_for_approval` WRITE;
/*!40000 ALTER TABLE `pending_for_approval` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_for_approval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_progress`
--

DROP TABLE IF EXISTS `students_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `students_progress` (
  `progress_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  PRIMARY KEY (`progress_id`),
  UNIQUE KEY `students_proggress_ak_1` (`user_id`,`lesson_id`),
  KEY `students_proggress_lessons` (`lesson_id`),
  CONSTRAINT `students_proggress_lessons` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`lesson_id`),
  CONSTRAINT `students_proggress_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_progress`
--

LOCK TABLES `students_progress` WRITE;
/*!40000 ALTER TABLE `students_progress` DISABLE KEYS */;
INSERT INTO `students_progress` VALUES (11,25,17),(12,25,18),(10,27,10),(13,27,11),(14,27,19),(21,29,12),(25,50,28),(37,50,36),(26,50,43),(36,50,44),(27,60,36),(28,60,37),(35,60,43),(29,61,28),(30,61,29),(31,61,30),(32,62,17),(33,62,36),(34,62,37),(38,65,43),(39,67,12),(40,68,28),(41,69,31),(42,71,43),(43,72,36),(44,74,36),(45,76,36),(46,77,28);
/*!40000 ALTER TABLE `students_progress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `email` varchar(50) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '1',
  `image_id` int(11) DEFAULT '1',
  `isAdmin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `users_ak_1` (`username`),
  KEY `users_images` (`image_id`),
  CONSTRAINT `users_images` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (24,'NerdyPenguinAdmin','{bcrypt}$2a$10$jxqYv5Odm0eoNjSTe0BPpupc/iOyOZLMuu2RpDajjnDdtijVxBp3K','newadmin@email.bg',1,34,1),(25,'BobRoss','{bcrypt}$2a$10$MijPKygWcTfODdhPokMLoO1oY/Ugalgp9FPZYwTXzC2r54Wup0dFi','BobRoss@painter.com',1,16,0),(27,'Juliana','{bcrypt}$2a$10$egjXcZdST3QU4IlM0NHzVOqqPPEhwB53gBUevy8K6whX1AR1zVKwy','JulianaMaster@yoga.com',1,15,0),(29,'christoph123','{bcrypt}$2a$10$RMuCGVFwBvrIaY/5c6Xf3uj79P2rgu4rTbNX7DswS1xrvq8Rl9Uai','hello@chrstphrddtz.com',1,6,0),(44,'CrashCourse','{bcrypt}$2a$10$mFi5mf5.8gETLwgYjcAAne2wsoj4kegaVrPS0QU5/jkCW4zQuqCGW','b7361919@urhen.com',1,20,0),(50,'pikachu','{bcrypt}$2a$10$Ty88/mb77FdFtyZkYgPBWuuNUSEq9YuBZqX9owNlMsiv8w2zbdfEi','pikachu@pokemon.com',1,14,0),(51,'SquattingSlav','{bcrypt}$2a$10$22hxMyuMFv5T0NmCXWdCTuchlitq7NVT6dzDSWFj.QDAGlTXmyE5S','SquattingSlav@sssr.ru',1,18,0),(52,'Kurzgesagt','{bcrypt}$2a$10$.xQ2i28OJ5w2nSP.Rqdrq.ecmxD.ZyqJyKQfpYapha2ZMDgrrU.pq','b7741254@urhen.com',1,19,0),(53,'MangoResearch','{bcrypt}$2a$10$QtarH6pF.sFVqpxI3GOZpunwphDiJJWDi4nyQXMfUQEVf9JNmDuR2','b7746906@urhen.com',1,2,0),(54,'LittlePony','{bcrypt}$2a$10$b.DuJWWl8.YnKUyJQozM7.wYc.QembMpjtf2Cjsja9q19qvE2LImm','b7880871@urhen.com',1,2,0),(55,'AndyGuitar','{bcrypt}$2a$10$7aXPC5jSxWOCmYFXSzFHE.cx9PhXflWf/QFlXk3wUnxoZBWiqett2','andy_guitar@brum.br',1,33,0),(56,'HackerRank','{bcrypt}$2a$10$SA7xxgFYMVBPUdn.YXD5EOo4bHY29SIZNeHqG0g9gBqcaRu2Tj1O2','b7883615@urhen.com',1,23,0),(57,'AsapSCIENCE','{bcrypt}$2a$10$WL70EMc1D6QWFWcovglMveOZ7sCG6f8Aij4T752xuerE4O.R.JXv6','asap@science.we',1,24,0),(58,'Hypnos','{bcrypt}$2a$10$5ri4C.TtlKl8Hfn3a.jiOenmKrsDLL2hxLbte7936i.se57AtYEgq','Hypnos@dieties.gr',1,25,0),(59,'CGMLab','{bcrypt}$2a$10$ugBH.bI0At6Zxx4VBPhRj.NxQ8TzD5hKFae9LR2FCYXZinyb52hL6','CGM@lab.org',1,2,0),(60,'MileKitich','{bcrypt}$2a$10$NkSd99x4Ex9YItt69ShVBOtcQuz/445KgDRyT.lEet7iSFoGsy616','mile@kitich.sr',1,26,0),(61,'ShakerMaker','{bcrypt}$2a$10$SxvB93yqkBcuWEtCYWQuO.2Mi/BfOUZgb0tFyA9Hus9BHDUPPQAC.','shaker@maker.nz',1,32,0),(62,'Naruto','{bcrypt}$2a$10$aHkXBirV8acucabzlObCnOX5y3HO6VjSc0.grfRRLHJWf5ryuiFj2','naruto@anime.com',1,30,0),(63,'pingvinche','{bcrypt}$2a$10$6Ihj73bLLy1rxF1Q./ZiwuTlMTmnLSX7i5pJAs8NpoJboOGf9CJXi','pingvinche@mail.com',1,2,0),(64,'pingvinT','{bcrypt}$2a$10$1d13LJnj4kR8MOsfm5DdxuXBu.sKm4lAw3E/HYKDTPYxKIexdUQmO','savion.jesse@ln0hio.com',1,2,0),(65,'TopBoss','{bcrypt}$2a$10$OvLWEYPn9NpSVMAc/1a8CeONkBZbc9nbZxweWejyV/e4eDpbAj5N.','vlad.carmyne@ln0hio.com',1,2,0),(66,'wannabe','{bcrypt}$2a$10$8Kj3G2NHKgrseOCvDHi6Teof0cffSH5Y0/FJ.9jY8WJfQ0eZaJqX6','prashant.mirza@ln0hio.com',0,2,0),(67,'bombateacher','{bcrypt}$2a$10$mXKZhVa9bW.O81EwUdgqDeMpsaH91raOkwR3I.j2uf/VugDeTU/nK','adeel.tyeler@ln0hio.com',1,2,0),(68,'MegaDaskal','{bcrypt}$2a$10$tGesGzIGzqu0c/sddsaN3OsrTz3Q9dYMmujB5VqphtEd6wP2J33ke','gage.traevon@ln0hio.com',1,2,0),(69,'djakuzitamakuzita','{bcrypt}$2a$10$l31UrIXjQBXMjTX2MtuYXeGpXcUo15W/uVgbVKvA./SIS5Q21Tow.','denver.nagi@ln0hio.com',1,2,0),(70,'newBoss','{bcrypt}$2a$10$E/CRmsbEWDlhEjO1wZz7wu6gGi.ZV8sRQs2tChSCmieSklKWVcmJC','newBoss@brrr.com',0,2,0),(71,'Hackerche','{bcrypt}$2a$10$l6rc8V0OU4L1xPGyKYPSROdzvMtX78wtp7STzvGiWeW3MJe0AG0Bu','salim.bam@ln0hio.com',1,2,0),(72,'Shef4e','{bcrypt}$2a$10$sDJFGrl7rnOluE9eOJBozexD7nUuSQq25hUOnGkD.UF3tliMalol6','jaber.lobsang@ln0hio.com',0,2,0),(73,'newBossBoss','{bcrypt}$2a$10$t1NSnfL6kVeD5vA3ULcfOOGcWZyRxbzhxbmdkDAZ8nx2ElVjaZ.eq','newBossBoss@mia.bg',1,3,1),(74,'NovTeacher','{bcrypt}$2a$10$1rgjbCcHc.Ls55iiwifa6.jF2y3VzF0e/MR.vgJVRD.mbt61y73gm','exu43529@oqiwq.com',1,2,0),(75,'BatVenci','{bcrypt}$2a$10$5BnuIYoFKgQ54Aoqu1c1PuKyOuI3N/2eBkAc91f5xWD88j6REzsN2','dqp50333@bcaoo.com',1,2,0),(76,'MadMax','{bcrypt}$2a$10$LM5OPYs77dN9pGkr9a2uWOAeJGnrzJHrMyQkIA5snJDkYuDka5QfS','byv07636@bcaoo.com',1,2,0),(77,'KakaZlatka','{bcrypt}$2a$10$vob4kkqm8Vv228PyXJsws.tQgz3US08HOwiCX8GgKRW60nisJ5KdK','hitadoz@vmailcloud.com',1,35,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_authorities`
--

DROP TABLE IF EXISTS `users_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users_authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `authority_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_authorities_authorities` (`authority_id`),
  KEY `users_authorities_users` (`user_id`),
  CONSTRAINT `users_authorities_authorities` FOREIGN KEY (`authority_id`) REFERENCES `authorities` (`authority_id`),
  CONSTRAINT `users_authorities_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_authorities`
--

LOCK TABLES `users_authorities` WRITE;
/*!40000 ALTER TABLE `users_authorities` DISABLE KEYS */;
INSERT INTO `users_authorities` VALUES (17,24,2),(18,25,3),(19,25,1),(21,27,3),(22,27,1),(24,29,3),(25,29,1),(41,44,3),(42,44,1),(49,50,3),(50,51,3),(51,51,1),(52,52,3),(53,52,1),(54,53,3),(55,53,1),(56,54,3),(57,55,3),(58,55,1),(59,56,3),(60,56,1),(61,57,3),(62,57,1),(63,58,3),(64,58,1),(65,59,3),(66,59,1),(67,60,3),(68,61,3),(69,62,3),(70,63,3),(71,64,3),(72,64,1),(73,65,3),(74,66,3),(75,66,1),(76,67,3),(77,68,3),(78,69,3),(79,70,3),(80,70,1),(81,71,3),(82,71,1),(83,72,3),(84,72,1),(85,73,2),(86,74,3),(87,74,1),(88,75,3),(89,76,3),(90,76,1),(91,77,3),(92,77,1);
/*!40000 ALTER TABLE `users_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `videos` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_title` varchar(100) NOT NULL,
  `video_url` text NOT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (17,'Bob Ross - A Walk in the Woods','https://www.youtube.com/embed/oh5p5f5_-7A'),(18,'Bob Ross - Mt. McKinley','https://www.youtube.com/embed/RInDWhYceLU'),(19,'Yin Yoga Fusion Flexibility','https://www.youtube.com/embed/-fac_iJ8Nw8'),(20,'Yin Yoga Muscle Release','https://www.youtube.com/embed/ICanFGTsW8c'),(21,'How a Film SLR Camera Works','https://www.youtube.com/embed/_3HcwTDFLx0'),(22,'Introduction to Film Formats','https://www.youtube.com/embed/VuMgoVHaAWg'),(24,'Film ISO - A beginners guide by ILFORD Photo','https://www.youtube.com/embed/AQ9rwLC8yqs'),(25,'The History of Life on Earth - Crash Course Ecology #1','https://www.youtube.com/embed/sjE-Pkjp3u4'),(26,'Population Ecology: The Texas Mosquito Mystery - Crash Course Ecology #2','https://www.youtube.com/embed/RBOsqmBQBQk'),(27,'Bob Ross - Ebony Sunset','https://www.youtube.com/embed/UOziR7PoVco'),(28,'Bob Ross - Winter Mist','https://www.youtube.com/embed/0pwoixRikn4'),(29,'Bob Ross - Quiet Stream','https://www.youtube.com/embed/DFSIQNjKRfk'),(34,'aaaaa','https://www.youtube.com/embed/hIowxzmCDpw'),(35,'Human Population Growth Crash Course Ecology','https://www.youtube.com/embed/E8dkWQVFAoA'),(36,'Community Ecology: Feel the Love - Crash Course Ecology','https://www.youtube.com/embed/GxE1SSqbSn4'),(37,'Eastern European Men School','https://www.youtube.com/embed/5Uzu58N-Sso'),(38,'What is Gopnik?','https://www.youtube.com/embed/Qif-Qz7NY48'),(39,'Russia Hardbass Crazy Dance','https://www.youtube.com/embed/y90yaLFoYoA'),(40,'Banking Explained – Money and Credit','https://www.youtube.com/embed/fTTGALaRZoc'),(41,'How The Stock Exchange Works','https://www.youtube.com/embed/F3QpgXBtDeo'),(42,'Universal Basic Income Explained – Free Money for Everybody? UBI','https://www.youtube.com/embed/kl39KHS07Xc'),(43,'Big Data Marketing','https://www.youtube.com/embed/XjmldAL9RQs'),(44,'Community Ecology II: Predators - Crash Course Ecology #5','https://www.youtube.com/embed/mFDiiSqGB7M'),(45,'Absolute Beginner? Start Here!','https://www.youtube.com/embed/5bTE5fbxDsc'),(46,'Ukulele Lesson 2','https://www.youtube.com/embed/0lS0io4K86Y'),(47,'Algorithms: Recursion','https://www.youtube.com/embed/KEEKn7Me-ms'),(48,'Ice Cream Parlor','https://www.youtube.com/embed/Ifwf3DBN1sc'),(49,'Algorithms: Quicksort','https://www.youtube.com/embed/SLauY6PpjW4'),(50,'The Science of Productivity','https://www.youtube.com/embed/lHfjvYzr-3g'),(51,'Office Etiquette 101 DOs and DON\'Ts','https://www.youtube.com/embed/MiYiREqjZes'),(52,'Why do we dream? - Amy Adkins','https://www.youtube.com/embed/2W85Dwxx218'),(53,'Brain Tricks - This Is How Your Brain Works','https://www.youtube.com/embed/JiTz2i4VHFw'),(54,'How to Make Lecture Videos','https://www.youtube.com/embed/-MneNkUk4e8'),(55,'How to set up a tent: 5 Easy Steps - A Camping Blog Series','https://www.youtube.com/embed/tAFPLSfDEKs');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-14 23:34:38
