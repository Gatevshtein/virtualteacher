# Nerdy Penguin

Vitual Teacher Web Application

The Nerdy Penguin virtual teaching platform provides students and teachers with a new interactive way of exchanging knowledge, skills and inspiration.

# Public Part

The home page is visible without authentification and features a catalog of the existing courses, ordered chronologically. The user can filter them by title, author or category and sort them alphabetically or by rating. If the user decides to further interact with the web app, he/she can choose to register as a teacher or a student.

![catalog](img/course_catalog.png)

# Private Part

**Students**, upon successful registration, will be allowed to enroll courses, watch lessons, download homework assignments and submit their projects to be graded. Educational materials will unlock only after the previous lesson has been completed and the corresponding homework is submitted. It will be then graded by the author of the course and if the student has recieved more than 60 points for all assignments,  the course is completed and the student can rate it.


![completed-course](img/completed_course.png)

**Teachers** do not have access to additional features automatically. Their applications are reviewed by the Nerdy Penguin admins, which approve or reject them. The candidates are informed via an email. Once approved, a teacher can create a course in 13 predefined categories. Lessons consist of a youtube video, homework assignment, and description. 

![completed-course](img/lesson.png)

# Admin part

 An admin can be created only through the admin panel. He/She decides if a user or a course should be deleted and manages teacher applications.