package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.CompletedCourse;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LearningProgressRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.LearningProgressServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class LearningProgressServiceImplTests {
    @Mock
   private CourseRepository courseMockRepository;
    @Mock
   private UserRepository userMockRepository;
    @Mock
   private LearningProgressRepository learningProgressMockRepository;
    @Mock
   private LessonRepository lessonMockRepository;
    @InjectMocks
   private LearningProgressServiceImpl learningProgressService;

    @Test
    public void hasUserCompletedACourse_Should_Invoke_Repo_With_TheSame_Parameters(){
        learningProgressService.hasUserCompletedCourse(1, 1);
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .hasUserCompletedCourse(1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isPreviousHomeworkSubmitted_Should_Throw_Exception_IfCourse_Does_Not_Exist(){
        Mockito.when(courseMockRepository.getCourse(1)).thenReturn(null);
        learningProgressService.isPreviousHomeworkSubmitted("SomeName", 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isPreviousHomeworkSubmitted_Should_Throw_Exception_IfLesson_Does_Not_Exist(){
        Mockito.when(lessonMockRepository.getLesson(1)).thenReturn(null);
        learningProgressService.isPreviousHomeworkSubmitted("SomeName", 1, 1);
    }

    @Test
    public void isPreviousHomeworkSubmitted_Should_ReturnTrue_If_Lesson_Is_First_From_Course(){
        Course course = new Course();
        Lesson lesson = new Lesson();
        course.setLessons(new ArrayList<Lesson>());
        course.getLessons().add(lesson);
        Mockito.when(courseMockRepository.getCourse(1)).thenReturn(course);
        Mockito.when(lessonMockRepository.getLesson(1)).thenReturn(lesson);
        Assert.assertTrue(learningProgressService.isPreviousHomeworkSubmitted
                ("SomeName", 1, 1));
    }

    @Test
    public void isPreviousHomeworkSubmitted_Should_InvokeRepo_If_Lesson_Is_NotFirst_From_Course(){
        Course course = new Course();
        Lesson lesson = new Lesson();
        lesson.setId(22);
        Lesson lesson2 = new Lesson();
        course.setLessons(new ArrayList<Lesson>());
        course.getLessons().add(lesson);
        course.getLessons().add(lesson2);
        AcademyUser user = new AcademyUser("Username", "password", "email");
        user.setUserId(1);
        Mockito.when(userMockRepository.getUserByUsername(user.getUsername())).thenReturn(user);
        Mockito.when(courseMockRepository.getCourse(1)).thenReturn(course);
        Mockito.when(lessonMockRepository.getLesson(1)).thenReturn(lesson2);
        learningProgressService.isPreviousHomeworkSubmitted(user.getUsername(), 1, 1);
        Mockito.verify(learningProgressMockRepository, Mockito.times
                (1)).wasHomeworkSubmitted(user.getUserId(), 22);
    }

    @Test(expected = IllegalArgumentException.class)
    public void enrollCourse_Should_ThrowException_When_Is_Already_Enrolled(){
        Course course = new Course();
        AcademyUser user = new AcademyUser();
        user.setEnrolledCourses(new HashSet<>());
        user.setUserId(1);
        course.setId(1);
        user.getEnrolledCourses().add(course);
        Mockito.when(userMockRepository.getUserById(user.getUserId())).thenReturn(user);
        Mockito.when(courseMockRepository.getCourse(course.getId())).thenReturn(course);
        learningProgressService.enrollCourse(1, 1);
    }

    @Test
    public void enrollCourse_Should_Invoke_Repository_If_Data_IsValid(){
        Course course = new Course();
        AcademyUser user = new AcademyUser();
        user.setEnrolledCourses(new HashSet<>());
        user.setUserId(1);
        course.setId(1);
        Mockito.when(userMockRepository.getUserById(user.getUserId())).thenReturn(user);
        Mockito.when(courseMockRepository.getCourse(course.getId())).thenReturn(course);
        learningProgressService.enrollCourse(1, 1);
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .enrollCourse(1,1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void gradeCourse_Should_Throw_Exception_If_Student_isNot_Pass_TheCourse(){
        Mockito.when(learningProgressMockRepository.findCompletedCourse(1, 1)).thenReturn(null);
        learningProgressService.gradeCourse(1, 1, 50);
    }

    @Test
    public void gradeCourse_Should_Invoke_Repo_If_CourseIsCompleted(){
        Mockito.when(learningProgressMockRepository.findCompletedCourse
                (1, 1)).thenReturn(new CompletedCourse());
        learningProgressService.gradeCourse(1, 1, 50);
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .gradeCourse(1, 1, 50);
    }

    @Test
    public void getAllCompletedCourses_Should_Invoke_Repo(){
        learningProgressService.getAllCompletedCourses(any(Integer.class));
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .getAllCompletedCourses(any(Integer.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCompletedLessonToUser_Should_Throw_Exception_If_User_Is_The_Author(){
        Course course = new Course();
        AcademyUser author = new AcademyUser();
        author.setUserId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        course.setAuthor(author);
        Mockito.when(lessonMockRepository.getLesson(lesson.getId())).thenReturn(lesson);
        learningProgressService.addCompletedLessonToUser(author, lesson.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCompletedLessonToUser_Should_Throw_Exception_If_User_DoesNot_Enroll_ForThe_Course(){
        Course course = new Course();
        AcademyUser student = new AcademyUser();
        AcademyUser teacher = new AcademyUser();
        student.setUserId(1);
        teacher.setUserId(2);
        student.setEnrolledCourses(new HashSet<>());
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        course.setAuthor(teacher);
        Mockito.when(lessonMockRepository.getLesson(lesson.getId())).thenReturn(lesson);
        learningProgressService.addCompletedLessonToUser(student, lesson.getId());
    }

    @Test
    public void addCompletedLessonToUser_Should_AddLesson_ToUserWatchedLesson_If_Data_Is_Valid(){
        Course course = new Course();
        AcademyUser student = new AcademyUser();
        AcademyUser teacher = new AcademyUser();
        student.setUserId(1);
        teacher.setUserId(2);
        student.setEnrolledCourses(new HashSet<>());
        student.getEnrolledCourses().add(course);
        student.setWatchedLessons(new HashSet<>());
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        course.setAuthor(teacher);
        Mockito.when(lessonMockRepository.getLesson(lesson.getId())).thenReturn(lesson);
        learningProgressService.addCompletedLessonToUser(student, lesson.getId());
        Assert.assertEquals(1, student.getWatchedLessons().size());
    }

    @Test
    public void AddCompletedLessonToUser_Should_Invoke_Repo_If_Data_Is_Valid(){
        Course course = new Course();
        AcademyUser student = new AcademyUser();
        AcademyUser teacher = new AcademyUser();
        student.setUserId(1); teacher.setUserId(2);
        student.setEnrolledCourses(new HashSet<>());
        student.getEnrolledCourses().add(course);
        student.setWatchedLessons(new HashSet<>());
        Lesson lesson = new Lesson(); lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        course.setAuthor(teacher);
        Mockito.when(lessonMockRepository.getLesson(lesson.getId())).thenReturn(lesson);
        learningProgressService.addCompletedLessonToUser(student, lesson.getId());
        Mockito.verify(userMockRepository, Mockito.times(1))
                .updateAcademyUser(student);
    }

    @Test
    public void getNumberOfEnrolledCourses_Should_Invoke_Repo(){
        learningProgressService.getNumberOfEnrolledCourses(any(Integer.class));
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .getNumberOfEnrolledCourses(any(Integer.class));
    }

    @Test
    public void getNumberOfCompletedCourses_Should_Invoke_Repo(){
        learningProgressService.getNumberOfCompletedCourses(any(Integer.class));
        Mockito.verify(learningProgressMockRepository, Mockito.times(1))
                .getNumberOfCompletedCourses(any(Integer.class));
    }

    @Test
    public void calculateUserProgressInCourse_Should_Return_Zero_If_Student_Is_Not_Watch_Any_Lessons(){
        AcademyUser student = new AcademyUser();
        student.setWatchedLessons(new HashSet<>());
        Course course = new Course();
        course.setId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setId(1);
        Assert.assertEquals(learningProgressService.calculateUserProgressInCourse(course, student), 0);
    }

    @Test
    public void calculateUserProgressInCourse_Should_Return_Valid_Result_If_User_Has_Watched_Lessons(){
        AcademyUser student = new AcademyUser();
        student.setWatchedLessons(new HashSet<>());
        Course course = new Course();
        course.setId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        student.getWatchedLessons().add(lesson);
        Assert.assertEquals(learningProgressService.calculateUserProgressInCourse(course, student), 100);
    }

    @Test
    public void getEnrolled_With_Progress_Should_Return_HashMap_With_User_Progress(){
        AcademyUser student = new AcademyUser();
        student.setWatchedLessons(new HashSet<>());
        Course course = new Course();
        course.setId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setCourse(course);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        student.getWatchedLessons().add(lesson);
        student.setEnrolledCourses(new HashSet<>());
        student.getEnrolledCourses().add(course);
        Assert.assertEquals
                (learningProgressService.getEnrolledWithProgress(student).size(), 1);
    }
}
