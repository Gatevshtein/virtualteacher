package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.TeacherServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class TeacherServiceImplTests {
    @Mock
    private UserRepository userMockRepo;

    @Mock
    private PasswordEncoder passwordMockEncoder;

    @Mock
    private CourseRepository courseMockRepo;

    @Mock
   private UserService userMockService;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @Test
    public void getAllTeachers_Should_Return_OnlyTeachers() {
        List<AcademyUser> users = initUsersData();
        Mockito.when(userMockRepo.getAllAcademyUsers()).thenReturn(users);
        Assert.assertEquals(teacherService.getAllTeachers().size(), 2);
    }

    @Test
    public void createTeacher_Should_Invoke_DefaultStudentRole_For_NewUser(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        teacherService.createNewTeacher(userDTO);
        Mockito.verify(userMockRepo, Mockito.times
                (1)).getRole("ROLE_STUDENT");
    }

    @Test
    public void createTeacher_Should_Invoke_DefaultImage(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        teacherService.createNewTeacher(userDTO);
        Mockito.verify(userMockRepo, Mockito.times(1))
                .getDefaultImage("ROLE_TEACHER");
    }

    @Test
    public void createTeacher_Should_Invoke_Repo_ToAddAccount_WaitingForApproval(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        teacherService.createNewTeacher(userDTO);
        Mockito.verify(userMockRepo, Mockito.times(1))
                .addAccountToWaitForApproval(any(AcademyUser.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUnsubmittedCoursesByAuthor_Should_Throw_AnException_If_UserId_Is_Invalid(){
        Mockito.when(userMockRepo.getUserById(1)).thenReturn(null);
        teacherService.getUnsubmittedCoursesByAuthor(1);
    }

    @Test
    public void getUnsubmittedCoursesByAuthor_Should_Invoke_Repo_If_UserExists(){
        AcademyUser teacher = new AcademyUser("Tosho", "password", "email");
        Mockito.when(userMockRepo.getUserById(1)).thenReturn(teacher);
        teacherService.getUnsubmittedCoursesByAuthor(1);
        Mockito.verify(courseMockRepo, Mockito.times(1)).getUnsubmittedCoursesByAuthor(teacher);
    }

    public List<AcademyUser> initUsersData() {
        Role studentRole = new Role("ROLE_STUDENT");
        Role teacherRole = new Role("ROLE_TEACHER");

        AcademyUser user1 = new AcademyUser
                ("Pesho", "teacher1pass", "user1@email.com");
        user1.getRoles().add(teacherRole);
        user1.setUserId(1);

        AcademyUser user2 = new AcademyUser
                ("Teacher2", "teacher2pass", "user2@email.com");
        user2.getRoles().add(teacherRole);
        user2.setUserId(2);

        AcademyUser user3 = new AcademyUser
                ("Student3", "student3pass", "user3@email.com");
        user3.getRoles().add(studentRole);
        user3.setUserId(3);

        AcademyUser user4 = new AcademyUser
                ("Student4", "student4pass", "user4@email.com");
        user4.getRoles().add(studentRole);
        user4.setUserId(4);

        List<AcademyUser> testUsers = new ArrayList<>();
        testUsers.add(user1);
        testUsers.add(user2);
        testUsers.add(user3);
        testUsers.add(user4);

        return testUsers;
    }
}
