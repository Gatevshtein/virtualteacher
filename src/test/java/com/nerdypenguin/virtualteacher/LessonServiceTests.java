package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.LessonDTO;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.VideoRepository;
import com.nerdypenguin.virtualteacher.services.LessonServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class LessonServiceTests {

    @Mock
    private CourseRepository courseRepository;
    @Mock
    private LessonRepository lessonRepository;
    @Mock
    private VideoRepository videoRepository;
    @Mock
    private StorageService storageService;

    @InjectMocks
    private LessonServiceImpl lessonService;


    @Test
    public void getLessonShouldReturnLessonWhenItExists() {
        Lesson lesson = getLesson();
        Mockito.when(lessonRepository.getLesson(1)).thenReturn(lesson);
        Assert.assertSame(lesson, lessonService.getLesson(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteLessonShouldThrowExceptionIfUserIsNotAuthor() {
        Lesson lesson = getLesson();
        Course course = new Course();
        lesson.setCourse(course);
        AcademyUser academyUser = getAcademyUserID1("TestUser", 1);
        AcademyUser fakeacademyUser = getAcademyUserID1("TestUser2", 2);
        course.setAuthor(academyUser);

        Mockito.when(lessonRepository.getLesson(1)).thenReturn(lesson);

        lessonService.deleteLesson(fakeacademyUser, lesson.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteLessonShouldThrowExceptionIfIsSubmitted() {
        Lesson lesson = getLesson();
        Course course = new Course();
        lesson.setCourse(course);
        AcademyUser academyUser = getAcademyUserID1("TestUser", 1);

        course.setAuthor(academyUser);
        course.setSubmitted(true);
        Mockito.when(lessonRepository.getLesson(1)).thenReturn(lesson);

        lessonService.deleteLesson(academyUser, lesson.getId());
    }

    @Test
    public void updateLessonShouldUpdateWhenValid() {
        Lesson lesson = getLesson();
        Course course = new Course();
        lesson.setCourse(course);
        AcademyUser academyUser = getAcademyUserID1("TestUser", 1);

        course.setAuthor(academyUser);
        course.setSubmitted(true);
        Mockito.when(lessonRepository.getLesson(1)).thenReturn(lesson);

        lessonService.updateLesson(academyUser, lesson.getId(), "New lesson description");

        Mockito.verify(lessonRepository, times(1)).updateLesson(lesson);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLessonToCourseShouldAddWhenValid() {
        AcademyUser academyUser = getAcademyUserID1("TestUser", 1);
        Course course = new Course();
        course.setAuthor(academyUser);
        course.setId(1);

        Mockito.when(courseRepository.getCourse(1)).thenReturn(course);
        Mockito.when(videoRepository.isVideoTitleFree("Video")).thenReturn(false);
        Video video = new Video("Video", "https://www.youtube.com/watch?v=IQpPdkd0B6M");
        LessonDTO lessonDTO = new LessonDTO();

        lessonDTO.setTitle("Title");
        lessonDTO.setLessonDescription("description");

        lessonService.addLessonToCourse(lessonDTO, course.getId(),
                video.getTitle(), video.getUrl(), "File.pdf", null);
     }

    @Test
    public void addLessonToCourseShouldCallRepository() {
        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setTitle("Title");
        lessonDTO.setLessonDescription("description");

        AcademyUser academyUser = getAcademyUserID1("TestUser", 1);
        Course course = new Course();
        course.setAuthor(academyUser);
        course.setId(1);
        MultipartFile file = new MockMultipartFile("File.pdf", "byte".getBytes());
        String fileName = "Homework_" + lessonDTO.getTitle()
                + course.getId() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
        Video video = new Video("Video", "https://www.youtube.com/watch?v=IQpPdkd0B6M");

        Mockito.when(courseRepository.getCourse(1)).thenReturn(course);
        Mockito.when(videoRepository.isVideoTitleFree("Video")).thenReturn(true);
        Mockito.doNothing().when(storageService).store(file, fileName);

        lessonService.addLessonToCourse(lessonDTO, course.getId(),
                video.getTitle(), video.getUrl(), "File.pdf", file);

        Lesson lesson = new Lesson(lessonDTO.getTitle().trim(), video, course,
                lessonDTO.getLessonDescription().trim(), fileName);

        Mockito.verify(courseRepository, times(1)).addLessonToCourse(lesson);

    }

    private Lesson getLesson() {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        return lesson;
    }

    private AcademyUser getAcademyUserID1(String testUser, int i) {
        AcademyUser academyUser = new AcademyUser(testUser, "TestPassword", "test@testing.com");
        academyUser.setUserId(i);
        return academyUser;
    }

}
