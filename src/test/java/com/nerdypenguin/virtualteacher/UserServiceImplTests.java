package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.*;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.UserServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userMockRepo;

    @Mock
    StorageService storageMockService;

    @Mock
    PasswordEncoder mockPassEncoder;

    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void getByUsername_Should_Return_User_When_Match_Exists(){
        AcademyUser expectedUser = initUsersData().get(0);
        Mockito.when(userMockRepo.getUserByUsername("Pesho")).thenReturn(expectedUser);
        AcademyUser foundUser = userService.getUserByUsername("Pesho");
        Assert.assertSame(expectedUser, foundUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByUsername_Should_Throw_Exception_When_Username_Doesnt_Exist(){

        Mockito.when(userMockRepo.getUserByUsername("Grisho")).thenReturn(null);
        userService.getUserByUsername("Grisho");
    }

    @Test
    public void updateSystemUser_Should_Call_Repo_WhenData_isValid(){

        SystemUser systemUser = new SystemUser("Gosho", "password", "email@gmail.com");
        Mockito.when(userMockRepo.getSystemUserByUsername("Gosho")).thenReturn(systemUser);
        UserDTO modelWithUpdates = new UserDTO("Sasho", "", "");
        userService.updateSystemUser("Gosho", modelWithUpdates);
        Mockito.verify(userMockRepo, Mockito.times(1)).updateSystemUser(systemUser);
    }

    @Test
    public void updateSystemUser_ShouldSet_NewUsername_When_Data_Is_Valid(){
        SystemUser systemUser = new SystemUser("Gosho", "password", "email@gmail.com");
        Mockito.when(userMockRepo.getSystemUserByUsername("Gosho")).thenReturn(systemUser);
        UserDTO modelWithUpdates = new UserDTO("Sasho", "", "");
        userService.updateSystemUser("Gosho", modelWithUpdates);
        Assert.assertEquals(systemUser.getUsername(), modelWithUpdates.getUsername());
    }

    @Test
    public void updateSystemUser_ShouldSet_NewEmail_When_Data_Is_Valid(){
        SystemUser systemUser = new SystemUser("Gosho", "password", "email@gmail.com");
        Mockito.when(userMockRepo.getSystemUserByUsername("Gosho")).thenReturn(systemUser);
        UserDTO modelWithUpdates = new UserDTO("", "", "newEmail@gmail.com");
        Mockito.when(userMockRepo.isEmailUsed(modelWithUpdates.getEmail())).thenReturn(false);
        userService.updateSystemUser("Gosho", modelWithUpdates);
        Assert.assertEquals(systemUser.getEmail(), modelWithUpdates.getEmail());
    }

    @Test
    public void UpdateSystemUser_ShouldSet_NewPassword_When_Data_Is_Valid(){
        SystemUser systemUser = new SystemUser("Gosho", "password", "email@gmail.com");
        Mockito.when(userMockRepo.getSystemUserByUsername("Gosho")).thenReturn(systemUser);
        UserDTO modelWithUpdates = new UserDTO("", "newPassword", "");
        Mockito.when(mockPassEncoder.encode(modelWithUpdates.getPassword())).thenReturn("1122334455");
        userService.updateSystemUser("Gosho", modelWithUpdates);
        Assert.assertEquals("1122334455" , systemUser.getPassword());
    }

    @Test
    public void UpdateProfilePicture_Should_Set_New_ProfilePicture(){
        byte[] mockArray = new byte[1];
        mockArray[0] = '#';
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        AcademyUser mockUser = new AcademyUser("Pesho", "password", "email");
        userService.updateProfilePicture(mockUser, mockFile);
        Assert.assertNotNull(mockUser.getImage());
    }

    @Test
    public void UpdateProfilePicture_Should_Invoke_Repo_For_Update(){
        byte[] mockArray = new byte[1];
        mockArray[0] = '#';
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        AcademyUser mockUser = new AcademyUser("Pesho", "password", "email");
        userService.updateProfilePicture(mockUser, mockFile);
        Mockito.verify(userMockRepo, Mockito.times(1)).updateAcademyUser(mockUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ValidateUsername_Should_Throw_AnException_When_Username_DoesNotMatch_Pattern(){
        String invalidUsername = "Ivan Geshev";
        userService.validateUsername(invalidUsername);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ValidateUsername_ShouldThrow_AnException_WhenUsername_IsNotFree(){
        String validUsername = "LemmyKillmister";
        Mockito.when(userMockRepo.isUsernameUsed(validUsername)).thenReturn(true);
        userService.validateUsername(validUsername);
    }

    @Test
    public void ValidateUsername_Should_NotThrow_AnException_When_Username_Matches_Pattern_And_IsFree(){
        String validUsername = "LemmyKillmister";
        Mockito.when(userMockRepo.isUsernameUsed(validUsername)).thenReturn(false);
        userService.validateUsername(validUsername);
    }

    @Test
    public void ValidateEmail_Should_NotThrow_AnException_WhenEmail_Matches_Pattern_And_IsFree(){
        String validEmail = "validEmail@gmail.com";
        Mockito.when(userMockRepo.isEmailUsed(validEmail)).thenReturn(false);
        userService.validateEmail(validEmail);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ValidateEmail_Should_Throw_AnException_WhenEmail_DoesNot_Match_Pattern(){
        String invalidEmail = "inv@lidEmail@gmail.com";
        userService.validateEmail(invalidEmail);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ValidateEmail_Should_Throw_AnException_WhenEmail_IsNot_Free(){
        String validEmail = "validEmail@gmail.com";
        Mockito.when(userMockRepo.isEmailUsed(validEmail)).thenReturn(true);
        userService.validateEmail(validEmail);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ValidatePassword_Should_Throw_AnException_Password_IsInvalid(){
        String invalidPassword = "Kofti Parola";
        userService.validatePassword(invalidPassword);
    }

    @Test
    public void ValidatePassword_ShouldNotThrow_AnException_When_Password_Is_Valid(){
        String validPassword = "pozdraviOtNap";
        userService.validatePassword(validPassword);
    }

    @Test(expected = ResponseStatusException.class)
    public void isFileValidImage_Should_Throw_Exception_If_File_Does_Not_Exist(){
        byte[] mockArray = new byte[1];
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        userService.isFileValidImage(mockFile);
    }

    public List<AcademyUser> initUsersData(){
        Role studentRole = new Role("ROLE_STUDENT");
        Role teacherRole = new Role("ROLE_TEACHER");

        AcademyUser user1 = new AcademyUser
                ("Pesho", "teacher1pass", "user1@email.com");
        user1.getRoles().add(teacherRole);
        user1.setUserId(1);

        AcademyUser user2 = new AcademyUser
                ("Teacher2", "teacher2pass", "user2@email.com");
        user2.getRoles().add(teacherRole);
        user2.setUserId(2);

        AcademyUser user3 = new AcademyUser
                ("Student3", "student3pass" , "user3@email.com");
        user3.getRoles().add(studentRole);
        user3.setUserId(3);

        AcademyUser user4 = new AcademyUser
                ("Student4", "student4pass", "user4@email.com");
        user4.getRoles().add(studentRole);
        user4.setUserId(4);

        List<AcademyUser> testUsers = new ArrayList<>();
        testUsers.add(user1);
        testUsers.add(user2);
        testUsers.add(user3);
        testUsers.add(user4);

        return testUsers;
    }

    public List<Course> initCourseData(){
        List<AcademyUser> users = initUsersData();
        List<Category> categories = initCategoryData();

        Course course1 = new Course("Course", users.get(0), categories.get(0), "SomeDescription");
        Course course2 = new Course("Course1", users.get(1), categories.get(1), "SomeDescription");
        course1.setId(1);
        course2.setId(2);

        Lesson lesson1 = new Lesson("Lesson1", new Video("Video1", "link1"),
                course1, "Description", "Homework1");
        lesson1.setId(1);

        Lesson lesson2 = new Lesson("Lesson2", new Video("Video2", "link2"),
                course1, "Description", "Homework2");
        lesson2.setId(2);

        Lesson lesson3 = new Lesson("Lesson3", new Video("Video3", "link3"),
                course2, "Description", "Homework3");
        lesson3.setId(3);

        Lesson lesson4 = new Lesson("Lesson4", new Video("Video4", "link4"),
                course2, "Description", "Homework4");
        lesson4.setId(4);

        Homework homework1 = new Homework(users.get(2), lesson1, "Homework", 80);
        Homework homework2 = new Homework(users.get(2), lesson2, "Homework", 90);
        Homework homework3 = new Homework(users.get(2), lesson3, "Homework", 30);
        Homework homework4 = new Homework(users.get(2), lesson4, "Homework", 10);

        Homework homework5 = new Homework(users.get(3), lesson1, "Homework", 0);
        Homework homework6 = new Homework(users.get(3), lesson2, "Homework", 0);

        List<Course> testCourses = new ArrayList<>();
        testCourses.add(course1);
        testCourses.add(course2);
        return testCourses;
    }

    public List<Category> initCategoryData(){

        Category design = new Category();
        Category business = new Category();

        design.setId(1);
        business.setId(2);

        design.setCategory("Design");
        business.setCategory("Business");

        design.setImage("catImage1");
        business.setImage("catImage2");

        List<Category> testCategories = new ArrayList<>();
        testCategories.add(design);
        testCategories.add(business);

        return testCategories;
    }

}
