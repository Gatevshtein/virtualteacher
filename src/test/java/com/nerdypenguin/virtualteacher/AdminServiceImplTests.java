package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.AdminServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.EmailService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplTests {
    @Mock
    private UserRepository mockUserRepo;
    @Mock
    private PasswordEncoder mockEncoder;
    @Mock
    private StorageService mockStorage;
    @Mock
    private EmailService mockEmailService;
    @Mock
   private UserService mockUserService;

    @InjectMocks
   private AdminServiceImpl adminService;

    private static final String APPROVE_MESSAGE =
            "Congrats! \n\nYour teacher account in 'Nerdy Penguin' is now active! " +
                    "\n\n Cheers!";

    private static final String REJECT_MESSAGE = "Sorry! \n\nYour application for a teacher account" +
            "in 'Nerdy Penguin' was rejected.\nYou are still welcome to use the student " +
            "functionality to its fullest! \n\nHave a nice day!";

    @Test
    public void getAdminByUsername_ShouldReturn_Admin_When_Admin_Exists(){
        SystemUser expectedUser = new SystemUser("Admin", "password", "email");
        Role adminRole = new Role("ROLE_ADMIN");
        expectedUser.setRoles(new HashSet<>());
        expectedUser.getRoles().add(adminRole);
        Mockito.when(mockUserRepo.getSystemUserByUsername(expectedUser.getUsername())).thenReturn(expectedUser);
        Mockito.when(mockUserRepo.getRole("ROLE_ADMIN")).thenReturn(adminRole);
        SystemUser foundUser = adminService.getSystemUserByUsername(expectedUser.getUsername());
        Assert.assertSame(expectedUser, foundUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAdminByUsername_Should_Throw_Exception_When_User_Is_Not_An_Admin(){
        SystemUser expectedUser = new SystemUser("Admin", "password", "email");
        Role adminRole = new Role("ROLE_ADMIN");
        expectedUser.setRoles(new HashSet<>());
        Mockito.when(mockUserRepo.getSystemUserByUsername(expectedUser.getUsername())).thenReturn(expectedUser);
        Mockito.when(mockUserRepo.getRole("ROLE_ADMIN")).thenReturn(adminRole);
        adminService.getSystemUserByUsername(expectedUser.getUsername());
    }

    @Test
    public void UpdateAdminPicture_Should_Set_New_ProfilePicture(){
        byte[] mockArray = new byte[1];
        mockArray[0] = '#';
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        SystemUser admin = new SystemUser("Pesho", "password", "email");
        adminService.updateAdminPicture(admin, mockFile);
        Assert.assertNotNull(admin.getImage());
    }

    @Test
    public void UpdateAdminPicture_Should_Invoke_Repo_For_Update(){
        byte[] mockArray = new byte[1];
        mockArray[0] = '#';
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        SystemUser mockUser = new SystemUser("Pesho", "password", "email");
        adminService.updateAdminPicture(mockUser, mockFile);
        Mockito.verify(mockUserRepo, Mockito.times(1)).updateSystemUser(mockUser);
    }


    @Test
    public void CreateNewAdmin_Should_Invoke_RepoForCreate_When_Data_Is_Valid(){
        UserDTO userDTO = new UserDTO("Pesho", "password", "email");
        adminService.createNewAdmin(userDTO);
        Mockito.verify(mockUserRepo, Mockito.times(1)).createSystemUser(any(SystemUser.class));
    }

    @Test
    public void GetAllPendingAccounts_Should_Invoke_Repo(){
        adminService.getAllPendingAccounts();
        Mockito.verify(mockUserRepo, Mockito.times(1)).getAllPendingAccounts();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ApproveUserAccount_Should_Throw_Exception_When_User_Does_Not_Exist(){
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(null);
        adminService.approveUserAccount(1);
    }

    @Test
    public void ApproveUserAccount_Should_Invoke_Repo_When_User_Exists() {
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn
                (new AcademyUser("Pesho", "password", "email"));
        adminService.approveUserAccount(1);
        Mockito.verify(mockUserRepo, Mockito.times(1)).approveTeacherAccount(1);
    }

    @Test
    public void ApproveUserAccount_Should_Invoke_EmailService_When_User_Exists() {
        AcademyUser academyUser = new AcademyUser("Pesho", "password", "pesho@email.bg");
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(academyUser);
        adminService.approveUserAccount(1);
        Mockito.verify(mockEmailService, Mockito.times(1))
                .sendEmail(academyUser.getEmail(), APPROVE_MESSAGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void RejectUserAccount_Should_Throw_Exception_When_User_Does_Not_Exist(){
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(null);
        adminService.rejectUserAccount(1);
    }

    @Test
    public void RejectUserAccount_Should_Invoke_Repo_When_User_Exists() {
        AcademyUser academyUser = new AcademyUser("Pesho", "password", "pesho@email.bg");
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(academyUser);
        adminService.rejectUserAccount(1);
        Mockito.verify(mockUserRepo, Mockito.times(1)).rejectTeacherAccount(academyUser);
    }

    @Test
    public void RejectUserAccount_Should_Invoke_EmailService_When_User_Exists() {
        AcademyUser academyUser = new AcademyUser("Pesho", "password", "pesho@email.bg");
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(academyUser);
        adminService.rejectUserAccount(1);
        Mockito.verify(mockEmailService, Mockito.times(1))
                .sendEmail(academyUser.getEmail(), REJECT_MESSAGE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void DeleteAcademyUser_Should_Throw_Exception_When_User_Does_Not_Exist(){
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(null);
        adminService.deleteAcademyUser(1);
    }

    @Test
    public void DeleteAcademyUser_Should_Invoke_Repo_When_User_Exists() {
        AcademyUser academyUser = new AcademyUser("Pesho", "password", "pesho@email.bg");
        Mockito.when(mockUserRepo.getUserById(1)).thenReturn(academyUser);
        adminService.deleteAcademyUser(1);
        Mockito.verify(mockUserRepo, Mockito.times(1)).deleteAcademyUser(1);
    }

}
