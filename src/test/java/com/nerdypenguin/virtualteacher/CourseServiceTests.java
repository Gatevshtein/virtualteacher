
package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Category;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTO;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.repositories.contracts.CategoryRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.CourseServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceTests {

    @Mock
    private CourseRepository courseRepository;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CourseServiceImpl courseService;

    @Test
    public void getCourseShouldReturnCourseWhenItExists() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course courseEcpected = new Course("TestCourse", academyUser, category, "Some test description here");
        courseEcpected.setId(1);
        Mockito.when(courseRepository.getCourse(1)).thenReturn(courseEcpected);
        Course courseActual = courseService.getCourse(1);
        Assert.assertSame(courseEcpected, courseActual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCourseShouldThrowExceptionWhenItDoestExist() {
        Mockito.when(courseRepository.getCourse(1)).thenReturn(null);
        courseService.getCourse(1);
    }

    @Test
    public void getAllShouldReturnAllCoursesWhichAreNotDeleted() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course course1 = new Course("TestCourse", academyUser, category, "Some test description here");
        course1.setId(1);
        Course course2 = new Course("TestCourse2", academyUser, category, "Some test description here2");
        course2.setId(2);
        Course course3 = new Course("TestCourse3", academyUser, category, "Some test description here3");
        course2.setId(3);

        List<Course> coursesExpected = new ArrayList<>(Arrays.asList(course1, course2, course3));
        Mockito.when(courseRepository.getAllCourses()).thenReturn(coursesExpected);

        List<Course> coursesActual = courseService.getAllCourses();

        Assert.assertEquals(coursesExpected, coursesActual);

    }

    @Test
    public void getCourseByTitleShouldReturnCourseIfItExists() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course coursesExpected = new Course("TestCourse", academyUser, category, "Some test description here");
        coursesExpected.setId(1);

        Mockito.when(courseRepository.getCourseByTitle("TestCourse")).thenReturn(coursesExpected);
        Course courseActual = courseService.getCourseByTitle("TestCourse");
        Assert.assertSame(coursesExpected, courseActual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCourseByTitleShouldThrowExceptionIfNotFound() {
        Mockito.when(courseRepository.getCourseByTitle("TestCourse")).thenReturn(null);
        courseService.getCourseByTitle("TestCourse");
    }

    @Test
    public void getAVGRatingOfCourse() {
        int expected = 42;
        Mockito.when(courseRepository.getCourseAVGRating(1)).thenReturn(expected);
        int actual = courseService.getCourseAVGRating(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void createCourseShouldCallRepository() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        category.setCategory("TestCategory");

        CourseDTO courseDTO = new CourseDTO("TestCourse", academyUser, category.getCategory(), "Some test description here");
        Course courseActual = new Course("TestCourse", academyUser, category, "Some test description here");

        Mockito.when(courseRepository.getCourseByTitle("TestCourse")).thenReturn(null);
        Mockito.when(userRepository.getUserByUsername("TestUser")).thenReturn(academyUser);
        Mockito.when(categoryRepository.getCategory("TestCategory")).thenReturn(category);

        courseService.createCourse(courseDTO, academyUser.getUsername());

        Mockito.verify(courseRepository,
                times(1)).createCourse(courseActual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCourseShouldThrowExceptionWhenCategoryIsNull() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        category.setCategory("TestCategory");

        CourseDTO courseDTO = new CourseDTO("TestCourse", academyUser, category.getCategory(), "Some test description here");

        Mockito.when(userRepository.getUserByUsername("TestUser")).thenReturn(academyUser);
        Mockito.when(categoryRepository.getCategory("TestCategory")).thenReturn(null);

        courseService.createCourse(courseDTO, academyUser.getUsername());
    }

    @Test
    public void deleteCourseShouldCallRepo() {
        Course course = new Course();
        courseService.deleteCourse(course);
        Mockito.verify(courseRepository,
                times(1)).deleteCourse(course);
    }

    @Test
    public void editCourseShouldCallRepoIfDataIsValid() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course course = new Course("TestCourse", academyUser, category, "Some test description here");
        course.setId(1);
        Mockito.when(courseRepository.getCourse(course.getId())).thenReturn(course);
        courseService.updateCourse(academyUser, course.getId(), "More test description here");

        Mockito.verify(courseRepository,
                times(1)).updateCourse(course);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editCourseShouldThrowExceptionIfUserIsNotAuthor() {
        AcademyUser author = new AcademyUser("TestUser", "testsPass", "test@java.com");
        author.setUserId(1);
        AcademyUser notAuthor = new AcademyUser("FakeTestUser", "testsPass", "test@java.com");
        notAuthor.setUserId(2);
        Course course = new Course("TestCourse", author, new Category(), "Some test description here");
        course.setId(1);

        Mockito.when(courseRepository.getCourse(course.getId())).thenReturn(course);
        courseService.updateCourse(notAuthor, course.getId(), "More test description here");

    }

    @Test(expected = IllegalArgumentException.class)
    public void submitCourseShouldThrowExceptionIfNoLessonsExist() {
        AcademyUser author = new AcademyUser("TestUser", "testsPass", "test@java.com");
        author.setUserId(1);
        Course course = new Course("TestCourse", author, new Category(), "Some test description here");
        course.setId(1);
        course.setLessons(new ArrayList<>());

        Mockito.when(userRepository.getUserById(author.getUserId())).thenReturn(author);
        Mockito.when(courseRepository.getCourse(course.getId())).thenReturn(course);

        courseService.submitCourse(course.getId(), author.getUserId());
    }

    @Test
    public void getCategoryImagesShouldCallCategoryRepo() {
        List<Category> categories = new ArrayList<>();
        Mockito.when(categoryRepository.getAllCategories()).thenReturn(categories);
        courseService.getCategoryImageSrc();
        Mockito.verify(categoryRepository,
                times(1)).getAllCategories();
    }

    @Test
    public void getFilteredCoursesShouldFilter() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course course1 = new Course("TestCourse", academyUser, category, "Some test description here");
        course1.setId(1);
        course1.setSubmitted(true);

        Course course2 = new Course("TestCourse2", academyUser, category, "Some test description here2");
        course2.setId(2);
        course2.setSubmitted(true);

        List<Course> courses = new ArrayList<>(Arrays.asList(course1, course2));

        Mockito.when(courseRepository.getCourseAVGRating(1)).thenReturn(30);
        Mockito.when(courseRepository.getCourseAVGRating(2)).thenReturn(55);
        Mockito.when(courseRepository.getFilteredCourses(null, null, "TestCourse"))
                .thenReturn(courses);

        CourseDTORating courseDTORating1 = new CourseDTORating(1, course1.getTitle(), academyUser, category, course1.getDescription(), true);
        CourseDTORating courseDTORating2 = new CourseDTORating(2, course2.getTitle(), academyUser, category, course2.getDescription(), true);

        List<CourseDTORating> expected = new ArrayList<>(Arrays.asList(courseDTORating1, courseDTORating2));
        List<CourseDTORating> coursesWithRatingActual = courseService
                .filteredCourses(null, null, "TestCourse");

        Assert.assertEquals(expected, coursesWithRatingActual);
    }

    @Test
    public void getAllCoursesShouldSortIfParamNotNull() {
        AcademyUser academyUser = new AcademyUser("TestUser", "testsPass", "test@java.com");
        Category category = new Category();
        Course course1 = new Course("TestCourse", academyUser, category, "Some test description here");
        course1.setId(1);
        course1.setSubmitted(true);

        Course course2 = new Course("TestCourse2", academyUser, category, "Some test description here2");
        course2.setId(2);
        course2.setSubmitted(true);

        List<Course> courses = new ArrayList<>(Arrays.asList(course1, course2));

        Mockito.when(courseRepository.getCourseAVGRating(1)).thenReturn(70);
        Mockito.when(courseRepository.getCourseAVGRating(2)).thenReturn(55);
        Mockito.when(courseRepository.getFilteredCourses(null, null, "TestCourse"))
                .thenReturn(courses);

        CourseDTORating courseDTORating1 = new CourseDTORating(1, course1.getTitle(), academyUser, category, course1.getDescription(), true);
        CourseDTORating courseDTORating2 = new CourseDTORating(2, course2.getTitle(), academyUser, category, course2.getDescription(), true);

        List<CourseDTORating> expected = new ArrayList<>(Arrays.asList(courseDTORating1, courseDTORating2));
        expected.sort(Comparator.comparing(CourseDTORating::getAvgRating));

        List<CourseDTORating> coursesWithRatingActual = courseService
                .getAllCourses(null, null, "TestCourse", "rating");

        Assert.assertEquals(expected, coursesWithRatingActual);

    }

    @Test(expected = IllegalArgumentException.class)
    public void submitCourseShouldThrowExceptionIfUserIsNotAuthor() {
        AcademyUser author = new AcademyUser("TestUser", "testsPass", "test@java.com");
        author.setUserId(1);
        AcademyUser notAuthor = new AcademyUser("TestUser2", "testsPass", "test2@java.com");
        author.setUserId(2);
        Course course = new Course("TestCourse", author, new Category(), "Some test description here");
        course.setId(1);
        course.setLessons(new ArrayList<>());

        Mockito.when(userRepository.getUserById(notAuthor.getUserId())).thenReturn(notAuthor);
        Mockito.when(courseRepository.getCourse(course.getId())).thenReturn(course);

        courseService.submitCourse(course.getId(), notAuthor.getUserId());

    }


    @Test
    public void submitCourseShouldSubmitIfValidData() {
        AcademyUser author = new AcademyUser("TestUser", "testsPass", "test@java.com");
        author.setUserId(1);
        Course course = new Course("TestCourse", author, new Category(), "Some test description here");
        course.setId(1);
        course.setLessons(new ArrayList<>(Arrays.asList(new Lesson(), new Lesson())));

        Mockito.when(userRepository.getUserById(author.getUserId())).thenReturn(author);
        Mockito.when(courseRepository.getCourse(course.getId())).thenReturn(course);

        course.setSubmitted(true);
        courseService.submitCourse(course.getId(), author.getUserId());

        Mockito.verify(courseRepository, times(1)).updateCourse(course);

    }

    @Test
    public void filteredCoursesShoudlReturnEmptyListIfAuthorNotFound() {

        List<CourseDTORating> expectedCourseDTORatings = new ArrayList<>();

        Mockito.when(userRepository.getUserByUsername("noAuthor")).thenReturn(null);

        List<CourseDTORating> courseDTORatings = courseService.filteredCourses(null, "noAuthor", null);

        Assert.assertEquals(expectedCourseDTORatings, courseDTORatings);

    }




}

