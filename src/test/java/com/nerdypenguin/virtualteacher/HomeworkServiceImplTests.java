package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.*;
import com.nerdypenguin.virtualteacher.repositories.contracts.HomeworkRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LearningProgressRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.HomeworkServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class HomeworkServiceImplTests {
    @Mock
    HomeworkRepository homeworkMockRepo;
    @Mock
    UserRepository userMockRepo;
    @Mock
    StorageService storageMockService;
    @Mock
    LessonRepository lessonMockRepo;
    @Mock
    LearningProgressRepository learningProgressMockRepo;

    @InjectMocks
    HomeworkServiceImpl homeworkService;

    @Test(expected = IllegalArgumentException.class)
    public void uploadStudentHomework_Should_Throw_Exception_If_User_Doesnt_Exist(){
        Lesson lesson = new Lesson();
        lesson.setId(1);
        Mockito.when(userMockRepo.getUserByUsername("Pesho")).thenReturn(null);
        Mockito.when(lessonMockRepo.getLesson(1)).thenReturn(lesson);
        homeworkService.uploadStudentHomework("Pesho", 1, any(MultipartFile.class));
    }
    @Test(expected = IllegalArgumentException.class)
    public void uploadStudentHomework_Should_Throw_Exception_If_Lesson_Doesnt_Exist(){
        AcademyUser user = new AcademyUser();
        Mockito.when(userMockRepo.getUserByUsername("Pesho")).thenReturn(user);
        Mockito.when(lessonMockRepo.getLesson(1)).thenReturn(null);
        homeworkService.uploadStudentHomework("Pesho", 1, any(MultipartFile.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void uploadStudentHomework_Should_ThrowException_If_UserIsNot_Enrolled_For_TheCourse(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        Lesson lesson = new Lesson();
        lesson.setId(1);
        Course course = new Course();
        lesson.setCourse(course);
        user.setEnrolledCourses(new HashSet<>());
        Mockito.when(userMockRepo.getUserByUsername("Pesho")).thenReturn(user);
        Mockito.when(lessonMockRepo.getLesson(1)).thenReturn(lesson);
        homeworkService.uploadStudentHomework("Pesho",1, any(MultipartFile.class));

    }

    @Test
    public void uploadStudentHomework_Should_Invoke_StorageService_If_Data_Is_Valid(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        Lesson lesson = new Lesson();
        lesson.setId(1);
        Course course = new Course();
        lesson.setCourse(course);
        user.setEnrolledCourses(new HashSet<>());
        user.getEnrolledCourses().add(course);
        Mockito.when(userMockRepo.getUserByUsername("Pesho")).thenReturn(user);
        Mockito.when(lessonMockRepo.getLesson(1)).thenReturn(lesson);
        byte[] mockArray = new byte[1];
        mockArray[0] = '#';
        MultipartFile mockFile = new MockMultipartFile("/mockPath", mockArray);
        homeworkService.uploadStudentHomework(user.getUsername(), lesson.getId(), mockFile);
        Mockito.verify(storageMockService, times(1))
                .store(any(MultipartFile.class), any(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getHomeworksWithoutGrade_Should_Throw_Exception_If_User_Is_Not_A_Teacher(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        Role role = new Role("ROLE_TEACHER");
        Mockito.when(userMockRepo.getUserById(1)).thenReturn(user);
        Mockito.when(userMockRepo.getRole("ROLE_TEACHER")).thenReturn(role);
        user.setRoles(new HashSet<>());
        homeworkService.getHomeworksWithoutGrade(1);
    }

    @Test
    public void getHomeworksWithoutGrade_Should_Invoke_Repo_If_User_Is_A_Teacher(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        Role role = new Role("ROLE_TEACHER");
        Mockito.when(userMockRepo.getUserById(1)).thenReturn(user);
        Mockito.when(userMockRepo.getRole("ROLE_TEACHER")).thenReturn(role);
        user.setRoles(new HashSet<>());
        user.getRoles().add(role);
        homeworkService.getHomeworksWithoutGrade(1);
        Mockito.verify(homeworkMockRepo, times(1))
                .getHomeworksWithoutGrade(1);
    }

    @Test
    public void getHomeworkById_Should_Invoke_Repo(){
        homeworkService.getHomeworkByID(1);
        Mockito.verify(homeworkMockRepo, times(1))
                .getHomeworkByID(1);
    }

    @Test
    public void getAvgGradeOfStudent_Should_Invoke_Repo(){
        homeworkService.getAVGradeOfStudent(1, 1);
        Mockito.verify(learningProgressMockRepo, times(1))
                .getAVGradeOfStudent(1, 1);
    }

    @Test
    public void getGradeOfHomework_Should_Return_Zero_If_Homework_WasNot_Submitted(){
      Assert.assertEquals(0, homeworkService.getGradeOfHomework(1, 1));
    }

    @Test
    public void getGradeOfHomework_Should_Invoke_Repo_If_Homeworks_Was_Submitted(){
        Mockito.when(learningProgressMockRepo.wasHomeworkSubmitted(1, 1)).thenReturn(true);
        homeworkService.getGradeOfHomework(1, 1);
        Mockito.verify(homeworkMockRepo, times(1))
                .getGradeOfHomework(1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void gradeHomework_Should_Throw_Exception_If_User_Is_Not_The_Author(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        user.setUserId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        Course course = new Course();
        lesson.setCourse(course);
        Homework homework = new Homework();
        homework.setHomework_id(1);
        homework.setLesson(lesson);
        course.setAuthor(new AcademyUser());
        Mockito.when(homeworkMockRepo.getHomeworkByID(1)).thenReturn(homework);
        homeworkService.gradeHomework(1, 20, 2);
    }

    @Test
    public void gradeHomework_Should_Invoke_RepoForGrade_If_User_Is_The_Author(){
        AcademyUser user = new AcademyUser("Pesho", "password", "email");
        user.setUserId(1);
        Lesson lesson = new Lesson();
        lesson.setId(1);
        Course course = new Course();
        lesson.setCourse(course);
        Homework homework = new Homework();
        homework.setHomework_id(1);
        homework.setLesson(lesson);
        homework.setStudent(user);
        course.setAuthor(user);
        course.setLessons(new ArrayList<>());
        course.getLessons().add(lesson);
        Mockito.when(homeworkMockRepo.getHomeworkByID(1)).thenReturn(homework);
        homeworkService.gradeHomework(1, 20, 1);
        Mockito.verify(homeworkMockRepo, times(1))
                .gradeHomework(1, 20);
    }


}
