package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.StudentServiceImpl;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceImplTests {

    @Mock
   private UserRepository userMockRepo;

    @Mock
   private PasswordEncoder passwordMockEncoder;

    @Mock
   private UserService mockUserService;

    @InjectMocks
   private StudentServiceImpl studentService;

    @Test
    public void getAllStudents_Should_Return_OnlyStudents(){
        List<AcademyUser> users = initUsersData();
        Mockito.when(userMockRepo.getAllAcademyUsers()).thenReturn(users);
        Mockito.when(userMockRepo.getRole("ROLE_STUDENT"))
                .thenReturn(new Role("ROLE_STUDENT"));
        Assert.assertEquals(studentService.getAllStudents().size(), 2);
    }

    @Test
    public void createStudent_Should_Invoke_StudentRole_For_NewUser(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        studentService.createNewStudent(userDTO);
        Mockito.verify(userMockRepo, Mockito.times
                (1)).getRole("ROLE_STUDENT");
    }

    @Test
    public void createStudent_Should_Invoke_DefaultImage(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        studentService.createNewStudent(userDTO);
        Mockito.verify(userMockRepo, Mockito.times(1))
                .getDefaultImage("ROLE_STUDENT");
    }

    @Test
    public void createStudent_Should_Invoke_CreateSystemUser_FromRepo(){
        UserDTO userDTO = new UserDTO("Stanka", "password", "email");
        studentService.createNewStudent(userDTO);
        Mockito.verify(userMockRepo, Mockito.times(1))
                .createSystemUser(any(SystemUser.class));
    }


    public List<AcademyUser> initUsersData(){
        Role studentRole = new Role("ROLE_STUDENT");
        Role teacherRole = new Role("ROLE_TEACHER");

        AcademyUser user1 = new AcademyUser
                ("Pesho", "teacher1pass", "user1@email.com");
        user1.getRoles().add(teacherRole);
        user1.setUserId(1);

        AcademyUser user2 = new AcademyUser
                ("Teacher2", "teacher2pass", "user2@email.com");
        user2.getRoles().add(teacherRole);
        user2.setUserId(2);

        AcademyUser user3 = new AcademyUser
                ("Student3", "student3pass" , "user3@email.com");
        user3.getRoles().add(studentRole);
        user3.setUserId(3);

        AcademyUser user4 = new AcademyUser
                ("Student4", "student4pass", "user4@email.com");
        user4.getRoles().add(studentRole);
        user4.setUserId(4);

        List<AcademyUser> testUsers = new ArrayList<>();
        testUsers.add(user1);
        testUsers.add(user2);
        testUsers.add(user3);
        testUsers.add(user4);

        return testUsers;
    }
}
