package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.repositories.contracts.VideoRepository;
import com.nerdypenguin.virtualteacher.services.VideoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.ArgumentMatchers.any;


@RunWith(MockitoJUnitRunner.class)
public class VideoServiceImplTests {
    @Mock
    VideoRepository videoMockRepo;
    @InjectMocks
    VideoServiceImpl videoService;

    @Test
    public void getVideoShouldInvoke_Repo(){
        videoService.getVideo(1);
        Mockito.verify(videoMockRepo, Mockito.times(1)).getVideo(1);
    }
    @Test
    public void saveVideo_Should_Invoke_Repo(){
        Video video = new Video();
        videoService.saveVideo(video);
        Mockito.verify(videoMockRepo, Mockito.times(1))
                .saveVideo(video);
    }
    @Test
    public void isVideoTitleFree_SHould_Invoke_Repo(){
        videoService.isVideoTitleFree("TItle");
        Mockito.verify(videoMockRepo, Mockito.times(1))
                .isVideoTitleFree("TItle");
    }
}
