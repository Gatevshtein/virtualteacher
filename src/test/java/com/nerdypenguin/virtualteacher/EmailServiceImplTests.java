package com.nerdypenguin.virtualteacher;

import com.nerdypenguin.virtualteacher.services.EmailServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplTests {

    @Mock
    JavaMailSender javaMailSender;

    @InjectMocks
    EmailServiceImpl emailService;

    @Test
    public void sendEmail_Should_Invoke_javaMailSender(){
        emailService.sendEmail("userEmail@gmail.com", "Maraba!");
        Mockito.verify(javaMailSender, Mockito.times(1))
                .send(any(SimpleMailMessage.class));
    }
}
