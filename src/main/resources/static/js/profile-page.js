$("#my-btn-show").click(function(){
    var val = $("#my-btn-show").val();
    if (val === "Hide") {
        $(".courseList").css("display", "none");
        $("#my-btn-show").val("Show");
    }else{
        $(".courseList").css("display", "block");
        $("#my-btn-show").val("Hide");
    }
});

$("#btn-edit").click(function(){
    var val = $("#btn-edit").val();
    if (val === "Edit") {
        $(".editForm").css("display", "block");
        $("#btn-edit").val("Cancel");
    }else{
        $(".editForm").css("display", "none");
        $("#btn-edit").val("Edit");
    }
});

$(function() {
    $('.material-card > .mc-btn-action').click(function () {
        var card = $(this).parent('.material-card');
        var icon = $(this).children('i');
        icon.addClass('fa-spin-fast');

        if (card.hasClass('mc-active')) {
            card.removeClass('mc-active');

            window.setTimeout(function() {
                icon
                    .removeClass('fa-arrow-left')
                    .removeClass('fa-spin-fast')
                    .addClass('fa-bars');

            }, 800);
        } else {
            card.addClass('mc-active');

            window.setTimeout(function() {
                icon
                    .removeClass('fa-bars')
                    .removeClass('fa-spin-fast')
                    .addClass('fa-arrow-left');

            }, 800);
        }
    });
});
