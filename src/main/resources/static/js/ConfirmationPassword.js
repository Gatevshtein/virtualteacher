var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity('');
    }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;


var pass = document.getElementById("password2")
    , confirm_password2 = document.getElementById("confirm_password2");

function validatePass(){
    if(pass.value != confirm_password2.value) {
        confirm_password2.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password2.setCustomValidity('');
    }
}
pass.onchange = validatePass;
confirm_password2.onkeyup = validatePass;