package com.nerdypenguin.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "courses")
@Where(clause = "isDeleted = false")
public class Course implements Comparable <Course> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private int id;

    @Column(name = "title")
    @Size(min = 5, max = 50, message = "Course title should be between 5 and 50 characters.")
    private String title;

    @ManyToOne
    @JoinColumn(name = "author_id")
    @JsonIgnore
    private AcademyUser author;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "course_description")
    @Size(min = 5, message = "Course description should be minimum 5 characters.")
    private String description;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "course")
    private List<Lesson> lessons;

    public static final int passGrade = 60;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    @Column(name = "isSubmitted")
    private boolean isSubmitted;

    public Course() {
    }

    public Course(@Valid String title, AcademyUser author, Category category, String description) {
        this.title = title;
        this.author = author;
        this.category = category;
        this.lessons = new ArrayList<>();
        this.description = description;
        this.isDeleted = false;
        this.isSubmitted = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public AcademyUser getAuthor() {
        return author;
    }

    public void setAuthor(AcademyUser author) {
        this.author = author;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return id == course.id &&
                Objects.equals(title, course.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

    public static int getPassGrade() {
        return passGrade;
    }

    public boolean isSubmitted() {
        return isSubmitted;
    }

    public void setSubmitted(boolean submitted) {
        isSubmitted = submitted;
    }

    @Override
    public int compareTo(Course o) {
        return this.title.compareTo(o.title);
    }
}