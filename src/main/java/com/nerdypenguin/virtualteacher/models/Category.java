package com.nerdypenguin.virtualteacher.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table (name = "categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private int id;

    @Column(name = "category")
    @Size(min = 3, max = 50, message = "Category name should be between 3 and 50 characters.")
    private String category;

    @Column(name = "category_image")
    @Size(min = 1, max = 100)
    private String image;

    public Category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
