package com.nerdypenguin.virtualteacher.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    private int imageId;

    @Column(name = "file_name")
    @Size(min = 1, max = 100)
    private String image;

    public Image(@Valid String image) {
        this.image = image;
    }

    public Image() {
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imageId=" + imageId +
                ", image='" + image + '\'' +
                '}';
    }
}
