package com.nerdypenguin.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Transactional
@Table(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="isAdmin",
        discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("1")
@Where(clause = "enabled = true")
public class SystemUser implements UserDetails{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "username")
    @Size(min = 5, max = 50, message = "Username should be between 5 and 50 symbols")
    private String username;

    @Column(name = "password")
    @Size(min = 5, max = 100, message = "Password should be between 5 and 50 symbols")
    private String password;

    @Column(name = "email")
    @NotNull
    private String email;

    @Column(name = "enabled")
    @NotNull
    private boolean enabled;

    @ManyToMany (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable (name = "users_authorities",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Role> roles;

    @OneToOne
    @JoinColumn(name = "image_id")
    @JsonIgnore
    private Image image;

    @Column(name = "isAdmin", insertable = false, updatable = false)
    private boolean isAdmin;

    public SystemUser(@Valid String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.enabled = true;
        this.roles = new HashSet<>();
        isAdmin = true;
    }

    public SystemUser() {
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Image getImage() {
        return image;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public Set<Role> getAuthorities() {
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

//    public String getImageForDisplay(){
//
//        try {
//            return Base64.getEncoder().encodeToString(image.getImage());
//        }catch (EncodingException e){
//            throw new IllegalArgumentException("Can't display the image");
//        }
//    }
}
