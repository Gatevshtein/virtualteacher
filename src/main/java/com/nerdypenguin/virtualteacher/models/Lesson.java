package com.nerdypenguin.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "lessons")
@Where(clause = "isDeleted = false")
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lesson_id")
    private int id;

    @Column(name = "lesson_title")
    @Size(min = 3, max = 50, message = "Lesson title should be between 3 and 50 characters")
    private String title;

    @ManyToOne
    @JoinColumn(name = "course_id")
    @JsonIgnore
    private Course course;

    @OneToOne(fetch = FetchType.EAGER , orphanRemoval = true)
    @JoinColumn(name = "video_id")
    private Video video;

    @Column(name = "lesson_description")
    @Size(min = 5, message = "Lesson description should be minimum 5 characters")
    private String lessonDescription;

    @Column(name = "homework_file_name")
    private String homeworkFileName;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    public Lesson() {
    }

    public Lesson(@Valid String title, Video video, Course course, String lessonDescription, String homeworkFileName) {
        this.title = title;
        this.video = video;
        this.course = course;
        this.lessonDescription = lessonDescription;
        this.homeworkFileName = homeworkFileName;
        this.isDeleted = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getLessonDescription() {
        return lessonDescription;
    }

    public void setLessonDescription(String lessonDescription) {
        this.lessonDescription = lessonDescription;
    }

    public String getHomeworkFileName() {
        return homeworkFileName;
    }

    public void setHomeworkFileName(String homeworkFileName) {
        this.homeworkFileName = homeworkFileName;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return id == lesson.id &&
                Objects.equals(title, lesson.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

}
