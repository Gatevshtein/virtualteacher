package com.nerdypenguin.virtualteacher.models;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "authorities")
public class Role
        implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authority_id")
    private int roleId;

    @Column(name = "authority")
    private String authority;

    public Role(String authority) {
        this.authority = authority;
    }

    public Role() {
    }

    public int getRoleId() {
        return roleId;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return roleId == role.roleId &&
                authority.equals(role.authority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, authority);
    }
}
