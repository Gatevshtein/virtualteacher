package com.nerdypenguin.virtualteacher.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;


@Entity
@Table(name = "course_ratings")
public class CompletedCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private int courseRating_id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private AcademyUser user;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "rate_by_student")
    @Max(value = 100, message = "Rating can be between 1 and 100 points.")
    private int rating;

    public CompletedCourse() {
    }

    public CompletedCourse(@Valid AcademyUser user, Course course, int rating) {
        this.user = user;
        this.course = course;
        this.rating = rating;
    }

    public int getCourseRating_id() {
        return courseRating_id;
    }

    public void setCourseRating_id(int course_id) {
        this.courseRating_id = course_id;
    }

    public AcademyUser getUser() {
        return user;
    }

    public void setUser(AcademyUser user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "CompletedCourse{" +
                "courseRating_id=" + courseRating_id +
                ", user=" + user +
                ", course=" + course +
                ", rating=" + rating +
                '}';
    }
}
