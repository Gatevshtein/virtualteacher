package com.nerdypenguin.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Entity
@DiscriminatorValue("0")
public class AcademyUser extends SystemUser {

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "enrolled_courses",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    @JsonIgnore
    private Set<Course> enrolledCourses;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "students_progress",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "lesson_id"))
    @JsonIgnore
    private Set<Lesson> watchedLessons;

    public AcademyUser(String username, String password, String email) {
        super(username, password, email);
        super.setAdmin(false);
        this.enrolledCourses = new TreeSet<>(new Comparator<Course>() {
            @Override
            public int compare(Course o1, Course o2) {
                return o1.getId() - o2.getId();
            }
        });
        this.watchedLessons = new HashSet<>();
    }

    public AcademyUser() {
    }

    public Set<Course> getEnrolledCourses() {
        return enrolledCourses;
    }

    public Set<Lesson> getWatchedLessons() {
        return watchedLessons;
    }

    public void setEnrolledCourses(Set<Course> enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }

    public void setWatchedLessons(Set<Lesson> watchedLessons) {
        this.watchedLessons = watchedLessons;
    }
}
