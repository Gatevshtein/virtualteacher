package com.nerdypenguin.virtualteacher.models;

import javax.persistence.*;

@Entity
@Table(name = "pending_for_approval")
public class PendingForApprovalRegistration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "teacher_id")
    private AcademyUser teacher;

    public PendingForApprovalRegistration(AcademyUser teacher) {
        this.teacher = teacher;
    }

    public PendingForApprovalRegistration() {
    }

    public int getId() {
        return id;
    }

    public AcademyUser getTeacher() {
        return teacher;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTeacher(AcademyUser teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "PendingForApprovalRegistration{" +
                "id=" + id +
                ", teacher=" + teacher +
                '}';
    }
}
