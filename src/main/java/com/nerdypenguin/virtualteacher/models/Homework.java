package com.nerdypenguin.virtualteacher.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;

@Entity
@Table(name = "homeworks")
public class Homework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "homework_id")
    private int homework_id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    private AcademyUser student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "lesson_id")
    private Lesson lesson;

    @Column(name = "file_name")
    private String fileName;

    @Column (name = "grade")
    @Max(value = 100, message = "Garde can be between 1 and 100 points")
    private int grade;

    @Column(name = "is_graded")
    private boolean isGraded;

    public Homework() {
    }

    public Homework(@Valid AcademyUser student, Lesson lesson, String fileName, int grade) {
        this.student = student;
        this.lesson = lesson;
        this.fileName = fileName;
        this.grade = grade;
    }

    public int getHomework_id() {
        return homework_id;
    }

    public void setHomework_id(int homework_id) {
        this.homework_id = homework_id;
    }

    public AcademyUser getStudent() {
        return student;
    }

    public void setStudent(AcademyUser student) {
        this.student = student;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName (String fileName) {
        this.fileName = fileName;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public boolean isGraded() {
        return isGraded;
    }

    public void setGraded(boolean graded) {
        isGraded = graded;
    }

    @Override
    public String toString() {
        return "Homework{" +
                "homework_id=" + homework_id +
                ", student=" + student +
                ", lesson=" + lesson +
                ", fileName='" + fileName + '\'' +
                ", grade=" + grade +
                ", isGraded=" + isGraded +
                '}';
    }
}
