package com.nerdypenguin.virtualteacher.models.DTOs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Lesson;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class CourseDTO {

    private int id;

    @Size(min = 5, max = 50, message = "Course title should be between 5 and 50 characters.")
    private String title;

    @JsonIgnore
    private AcademyUser author;

    private String category;

    @Column(name = "course_description")
    @Size(min = 5, message = "Course description should be minimum 5 characters.")
    private String description;

    private List<Lesson> lessons;

    private boolean isDeleted;

    public CourseDTO() {
    }

    public CourseDTO(@Valid String title, AcademyUser author, String category, String description) {
        this.title = title;
        this.author = author;
        this.category = category;
        this.lessons = new ArrayList<>();
        this.description = description;
        this.lessons = new ArrayList<>();
        this.isDeleted = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AcademyUser getAuthor() {
        return author;
    }

    public void setAuthor(AcademyUser author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

}
