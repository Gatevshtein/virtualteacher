package com.nerdypenguin.virtualteacher.models.DTOs;

import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Video;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LessonDTO {

    private int id;

    @Size(min = 3, max = 50, message = "Lesson title should be between 3 and 50 characters")
    private String title;

    private Course course;

    private Video video;

    @Size(min = 5, message = "Lesson description should be minimum 5 characters long")
    private String lessonDescription;

    @NotNull
    private String homeworkFileName;

    private boolean isDeleted;

    public LessonDTO() {
    }

    public LessonDTO(@Valid String title, Course course, Video video, String lessonDescription, String homeworkFileName) {
        this.title = title;
        this.course = course;
        this.video = video;
        this.lessonDescription = lessonDescription;
        this.homeworkFileName = homeworkFileName;
        this.isDeleted =false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getLessonDescription() {
        return lessonDescription;
    }

    public void setLessonDescription(String lessonDescription) {
        this.lessonDescription = lessonDescription;
    }

    public String getHomeworkFileName() {
        return homeworkFileName;
    }

    public void setHomeworkFileName(String homeworkFileName) {
        this.homeworkFileName = homeworkFileName;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
