package com.nerdypenguin.virtualteacher.models.DTOs;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDTO {

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 50, message = "Username should be between 5 and 50 symbols")
    private String username;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 50, message = "Password should be between 5 and 50 symbols")
    private String password;

    @NotNull
    @NotEmpty
    private String email;

    public UserDTO() {
    }

    public UserDTO(@Valid String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
