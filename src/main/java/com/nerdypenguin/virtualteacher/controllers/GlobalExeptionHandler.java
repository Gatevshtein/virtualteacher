package com.nerdypenguin.virtualteacher.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class GlobalExeptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = IllegalArgumentException.class)
    public String handleError(HttpServletRequest request, Exception e, Model model) {
        model.addAttribute("e", e.getMessage());
        return "error";
    }

    @ExceptionHandler(value = ResponseStatusException.class)
    public String handleResponseStatus(HttpServletRequest request, Exception e, Model model) {
        model.addAttribute("e", e.getMessage());
        return "error";
    }

    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public String handleMultipartException (HttpServletRequest request, Exception e, Model model) {
        model.addAttribute("e", "File should be up to 5 MB");
        return "error";
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public String handleValidationException ( ConstraintViolationException e, Model model){
StringBuilder stringBuilder = new StringBuilder();
        for (ConstraintViolation cv: e.getConstraintViolations()) {
           stringBuilder.append(cv.getMessage());
           stringBuilder.append("\n");
        }
        model.addAttribute("e", stringBuilder.toString());
        return "error";
    }
}
