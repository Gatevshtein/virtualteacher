package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import com.nerdypenguin.virtualteacher.services.contracts.LearningProgressService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

@Controller
public class CourseDetailsController {

    private CourseService courseService;
    private UserService userService;
    private LearningProgressService learningProgressService;
    private StorageService storageService;

    private boolean hasUserCompletedThisCourse;
    private boolean isUserCurrentlyEnrolled;


    @Autowired
    public CourseDetailsController(CourseService courseService, UserService userService,
                                   LearningProgressService learningProgressService,
                                   StorageService storageService) {
        this.courseService = courseService;
        this.userService = userService;
        this.learningProgressService = learningProgressService;
        this.storageService = storageService;
    }

    @GetMapping("/{courseID}/details")
    @PreAuthorize("isAuthenticated()")
    public String showCoursePage(@PathVariable int courseID, Model model) {
        AcademyUser academyUser = getAuthenticatedUser();
        checkStudentCourseStatus(academyUser, courseID);
        Course course = courseService.getCourse(courseID);

        if (!course.isSubmitted() && course.getAuthor().getUserId() != academyUser.getUserId()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "This course is still in construction");
        }

        model.addAttribute("authorImage", getEncodedImage(course.getAuthor()));
        model.addAttribute("categoryImage", courseService.getCategoryImageSrc().get(course.getCategory().getId()));
        model.addAttribute("course", course);
        model.addAttribute("academyUser", academyUser);
        model.addAttribute("courseAVGRating", courseService.getCourseAVGRating(courseID));
        model.addAttribute("hasUserCompletedThisCourse", hasUserCompletedThisCourse);
        model.addAttribute("isUserCurrentlyEnrolled", isUserCurrentlyEnrolled);

        return "coursedetails";

    }

    @PostMapping("/{courseID}/details")
    @PreAuthorize("hasAuthority('ROLE_STUDENT')")
    public String courseEnroll(@PathVariable int courseID,
                               Model model) {

        AcademyUser academyUser = getAuthenticatedUser();
        Course course = courseService.getCourse(courseID);
        learningProgressService.enrollCourse(academyUser.getUserId(), courseID);

        return "redirect:/" + courseID + "/lessons/" + course.getLessons().get(0).getId();
    }

    @PutMapping("/{courseID}/details")
    @PreAuthorize("hasAuthority('ROLE_STUDENT')")
    public String courseRate(@PathVariable int courseID,
                             @RequestParam(required = false, defaultValue = "0") int rating) {

        AcademyUser academyUser = getAuthenticatedUser();
        learningProgressService.gradeCourse(academyUser.getUserId(), courseID, rating);

        return "redirect:/" + courseID + "/details";
    }

    @DeleteMapping("/{courseID}/details")
    public String deleteCourse(@PathVariable int courseID) {

        AcademyUser academyUser = getAuthenticatedUser();

        if (academyUser.getUserId() == courseService.getCourse(courseID).getAuthor().getUserId()) {
            courseService.deleteCourse(courseService.getCourse(courseID));
            return "redirect:/newcourse";
        } else
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Only authors and admins can delete a course");
    }

    @PutMapping("/{courseID}/details/submit")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String courseSubmit(@PathVariable int courseID) {

        AcademyUser academyUser = getAuthenticatedUser();
        courseService.submitCourse(courseID, academyUser.getUserId());

        return "redirect:/" + courseID + "/details";
    }

    private void checkStudentCourseStatus(AcademyUser academyUser, int courseID) {
        hasUserCompletedThisCourse = learningProgressService
                .hasUserCompletedCourse(academyUser.getUserId(), courseID);
        isUserCurrentlyEnrolled = academyUser
                .getEnrolledCourses()
                .contains(courseService.getCourse(courseID));
    }

    @PutMapping("/{courseID}/details/edit")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String editCourseDescription(@PathVariable int courseID,
                                        @RequestParam String description) {
        AcademyUser academyUser = getAuthenticatedUser();
        courseService.updateCourse(academyUser, courseID, description);
        return "redirect:/" + courseID + "/details";
    }

    private AcademyUser getAuthenticatedUser() {
        return userService.getUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }

    private String getEncodedImage(AcademyUser academyUser) {
        try {
            File image = storageService.loadAsResource(academyUser.getImage().getImage()).getFile();
            return "data:image/png;base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(image.toPath()));
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The requested data was not found");
        }
    }

}
