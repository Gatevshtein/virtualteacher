package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.LessonDTO;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import com.nerdypenguin.virtualteacher.services.contracts.LessonService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class CreateLessonController {

    private CourseService courseService;
    private LessonService lessonService;
    private UserService userService;

    @Autowired
    public CreateLessonController(CourseService courseService,
                                  LessonService lessonService,
                                  UserService userService) {
        this.courseService = courseService;
        this.lessonService = lessonService;
        this.userService = userService;
    }

    @GetMapping("/{courseID}/newlesson")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String showCreateLesson(@PathVariable int courseID, Model model) {

        Course course = courseService.getCourse(courseID);

        if (getAuthenticatedUser().getUserId() != course.getAuthor().getUserId()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Only the author of the course can amend its content");
        }

        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setCourse(course);
        model.addAttribute("lessonDTO", lessonDTO);

        return "newlesson";
    }

    @PostMapping("/{courseID}/newlesson")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String createLesson(@PathVariable int courseID, @ModelAttribute LessonDTO lessonDTO,
                               @RequestParam String videoTitle,
                               @RequestParam String videoURL,
                               @RequestParam MultipartFile file) {

        lessonService.addLessonToCourse(lessonDTO,
                courseID,
                videoTitle,
                videoURL,
                file.getName(),
                file);

        return "redirect:/" + courseID + "/newlesson";
    }

    private AcademyUser getAuthenticatedUser() {
        return userService.getUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }
}
