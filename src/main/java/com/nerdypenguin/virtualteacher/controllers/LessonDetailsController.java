package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

@Controller
public class LessonDetailsController {

    private CourseService courseService;
    private HomeworkService homeworkService;
    private UserService userService;
    private final StorageService storageService;
    private LearningProgressService learningProgressService;
    private LessonService lessonService;

    private boolean isLessonCompleted;
    private boolean isPreviousHomeworkSubmitted;
    private boolean isStudentEnrolled;


    @Autowired
    public LessonDetailsController(CourseService courseService,
                                   HomeworkService homeworkService,
                                   UserService userService,
                                   StorageService storageService,
                                   LearningProgressService learningProgressService,
                                   LessonService lessonService) {
        this.courseService = courseService;
        this.homeworkService = homeworkService;
        this.storageService = storageService;
        this.userService = userService;
        this.learningProgressService = learningProgressService;
        this.lessonService = lessonService;
    }

    @GetMapping("/{courseID}/lessons/{lessonID}")
    @PreAuthorize("hasAuthority('ROLE_STUDENT')")
    public String showCoursePage(@PathVariable int courseID, @PathVariable int lessonID, Model model) throws IOException {

        Course course = courseService.getCourse(courseID);
        Lesson lesson = lessonService.getLesson(lessonID);
        AcademyUser academyUser = getAuthenticatedUser();

        checkIfUserCanSeeTheCourse(course, academyUser);
        checkStudentCourseProgress(academyUser, lessonID, courseID);

        int orderOfLessonInCourse = course.getLessons().indexOf(lesson);

        model.addAttribute("authorImage", getEncodedImage(course.getAuthor()));
        model.addAttribute("academyUser", academyUser);
        model.addAttribute("lesson", lesson);
        model.addAttribute("grade", homeworkService.getGradeOfHomework(academyUser.getUserId(), lessonID));

        model.addAttribute("isStudentEnrolled", isStudentEnrolled);
        model.addAttribute("isLessonCompleted", isLessonCompleted);
        model.addAttribute("isPreviousHomeworkSubmitted", isPreviousHomeworkSubmitted);

        model.addAttribute("lessonIndex", orderOfLessonInCourse + 1);
        model.addAttribute("courseTotalLessons", course.getLessons().stream().distinct().count());

        return "lessondetails";
    }

    @GetMapping("/{lessonID}/gethomework")
    @PreAuthorize("hasAuthority('ROLE_STUDENT')")
    @ResponseBody
    ResponseEntity<Resource> serveFile(@PathVariable int lessonID) {

        Lesson lesson = lessonService.getLesson(lessonID);
        AcademyUser academyUser = getAuthenticatedUser();

        checkIfStudentIsEnrolled(lesson, academyUser);

        Resource file = storageService.loadAsResource(lesson.getHomeworkFileName());
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/{courseID}/lessons/{lessonID}")
    @PreAuthorize("hasAuthority('ROLE_STUDENT')")
    public String uploadHomework(@PathVariable int courseID,
                                 @PathVariable int lessonID,
                                 @RequestParam MultipartFile file) {

        AcademyUser academyUser = getAuthenticatedUser();
        checkStudentCourseProgress(academyUser, lessonID, courseID);

        if (isLessonCompleted) {
            homeworkService.uploadStudentHomework(academyUser.getUsername(), lessonID, file);
            return "redirect:/" + courseID + "/lessons/" + lessonID;
        } else
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                "Please finish lesson first");
    }

    @PutMapping("/{courseID}/lessons/{lessonID}")
    public String markAsWatched(@PathVariable int courseID,
                                @PathVariable int lessonID) {

        AcademyUser academyUser = getAuthenticatedUser();
        checkStudentCourseProgress(academyUser, lessonID, courseID);

        if (isPreviousHomeworkSubmitted) {
            learningProgressService.addCompletedLessonToUser(academyUser, lessonID);
            return "redirect:/" + courseID + "/lessons/" + lessonID;
        } else
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Please submit homework for previous lesson!");
    }

    @DeleteMapping("/{courseID}/lessons/{lessonID}")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String deleteLesson(@PathVariable int courseID,
                               @PathVariable int lessonID) {
        AcademyUser academyUser = getAuthenticatedUser();
        lessonService.deleteLesson(academyUser, lessonID);
        return "redirect:/" + courseID + "/details";
    }

    @PutMapping("/{courseID}/lessons/{lessonID}/edit")
    public String editDescription(@PathVariable int courseID,
                                  @PathVariable int lessonID,
                                  @RequestParam String description) {

        AcademyUser academyUser = getAuthenticatedUser();
        lessonService.updateLesson(academyUser, lessonID, description);
        return "redirect:/" + courseID + "/lessons/" + lessonID;

    }

    private void checkIfUserCanSeeTheCourse(Course course, AcademyUser academyUser) {
        if (!course.isSubmitted() && course.getAuthor().getUserId() != academyUser.getUserId()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "This course is still in construction");
        }
    }

    private void checkStudentCourseProgress(AcademyUser academyUser, int lessonID, int courseID) {

        Lesson lesson = lessonService.getLesson(lessonID);

        isLessonCompleted = academyUser.getWatchedLessons().contains(lesson);
        isPreviousHomeworkSubmitted = learningProgressService.isPreviousHomeworkSubmitted(academyUser.getUsername(),
                lessonID,
                courseID);
        isStudentEnrolled = academyUser.getEnrolledCourses().contains(courseService
                .getCourse(courseID));

    }

    private void checkIfStudentIsEnrolled(Lesson lesson, AcademyUser academyUser) {
        if (!academyUser.getEnrolledCourses().contains(lesson.getCourse())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Please enroll the course");
        }
    }

    private AcademyUser getAuthenticatedUser() {
        return userService.getUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }

    private String getEncodedImage(AcademyUser academyUser) {
        try {
            File image = storageService.loadAsResource(academyUser.getImage().getImage()).getFile();
            return "data:image/png;base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(image.toPath()));
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The requested data was not found");
        }
    }
}
