package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.models.Homework;
import com.nerdypenguin.virtualteacher.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
public class JudgeController {
    private HomeworkService homeworkService;
    private UserService userService;
    private CourseService courseService;
    private TeacherService teacherService;
    private final StorageService storageService;

    @Autowired
    public JudgeController(HomeworkService homeworkService, UserService userService,
                           CourseService courseService,
                              StorageService storageService, TeacherService teacherService) {
        this.homeworkService = homeworkService;
        this.userService = userService;
        this.courseService = courseService;
        this.teacherService = teacherService;
        this.storageService = storageService;
    }

    @GetMapping("/judge")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String showJudgePage(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        AcademyUser teacher = userService.getUserByUsername(username);
        List<Homework> ungradedHomeworks = homeworkService.getHomeworksWithoutGrade(teacher.getUserId());
        model.addAttribute("homeworks", ungradedHomeworks);
        return "judge";
    }

    @GetMapping("/{homeworkID}/getStudentHomework")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    @ResponseBody
    ResponseEntity<Resource> serveFile(@PathVariable int homeworkID){
        Homework homework = homeworkService.getHomeworkByID(homeworkID);
        Resource file = storageService.loadAsResource(homework.getFileName());
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/{homeworkID}/grade")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String gradeHomework(@RequestParam int grade, @PathVariable int homeworkID){
        try {
            AcademyUser teacher = getAuthenticatedUser();
            homeworkService.gradeHomework(homeworkID, grade, teacher.getUserId());

        }catch (IllegalArgumentException iae){
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, iae.getMessage());
        }
        return "redirect:/judge";
    }

    @GetMapping("/judge/courses")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String getCoursesByAuthor(Model model){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<CourseDTORating> allCoursesOfAnAuthor = courseService
                .filteredCourses(null, username, null);
        model.addAttribute("courses", allCoursesOfAnAuthor);

        return "createdCourses";
    }

    private AcademyUser getAuthenticatedUser() {
        return userService.getUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }
}