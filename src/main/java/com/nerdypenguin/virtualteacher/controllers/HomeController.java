package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Controller
@RequestMapping("/")
public class HomeController {

    private CourseService courseService;
    private StorageService storageService;

    @Autowired
    public HomeController(CourseService courseService, StorageService storageService) {
        this.courseService = courseService;
        this.storageService = storageService;
    }

    @GetMapping
    public String showHomePage(Model model,
                               @RequestParam(required = false) String category,
                               @RequestParam(required = false) String author,
                               @RequestParam(required = false) String courseName,
                               @RequestParam(required = false) String sortBy,
                               @RequestParam(name = "page", required = false) Integer currentPage,
                               @RequestParam(name = "size", required = false) Integer pageSize) {
        if (currentPage == null) {
            currentPage = 1;
        }
        if (pageSize == null) {
            pageSize = 8;
        }

        List<CourseDTORating> courses = courseService.getAllCourses(category, author, courseName, sortBy);

        model.addAttribute("courses", courses);
        model.addAttribute("categoryImages", courseService.getCategoryImageSrc());
        model.addAttribute("authorImages", getEncodedImages(courses));

        Page<CourseDTORating> coursePage = courseService.findPaginated(PageRequest.of(currentPage - 1, pageSize), courses);
        model.addAttribute("coursePage", coursePage);

        int totalPages = coursePage.getTotalPages();

        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "index";
    }


    private Map<Integer, String> getEncodedImages(List<CourseDTORating> list) {
        try {
            Map<Integer, String> encodedUserImages = new HashMap<>();
            for (CourseDTORating c : list) {
                File image = storageService.loadAsResource(c.getAuthor().getImage().getImage()).getFile();
                String imageEncoded = "data:image/png;base64," + Base64.getEncoder().encodeToString(Files.readAllBytes(image.toPath()));
                encodedUserImages.put(c.getAuthor().getUserId(), imageEncoded);
            }
            return encodedUserImages;
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The requested data was not found");
        }
    }
}
