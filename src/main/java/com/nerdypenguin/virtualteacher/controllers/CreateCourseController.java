package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTO;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CreateCourseController {

    private CourseService courseService;

    @Autowired
    public CreateCourseController(CourseService courseService) {
        this.courseService = courseService;

    }

    @GetMapping("/newcourse")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String showNewCoursePage(Model model) {
        model.addAttribute("courseDTO", new CourseDTO());
        return "newcourse";
    }

    @PostMapping("/newcourse")
    @PreAuthorize("hasAuthority('ROLE_TEACHER')")
    public String createCourse(@ModelAttribute CourseDTO courseDTO,
                               BindingResult bindingResult) {

        String username = SecurityContextHolder
                .getContext()
                .getAuthentication().getName();

        courseService.createCourse(courseDTO, username);
        Course course = courseService.getCourseByTitle(courseDTO.getTitle());

        return "redirect:/" + course.getId() + "/newlesson";
    }

}
