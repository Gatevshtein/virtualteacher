package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sun.net.www.protocol.http.AuthenticationInfo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class ProfileController {
    private UserService userService;
    private StorageService storageService;
    private CourseService courseService;
    private LearningProgressService learningProgressService;
    private TeacherService teacherService;
    private HomeworkService homeworkService;

    @Autowired
    public ProfileController(UserService userService,
                             StorageService storageService,
                             CourseService courseService,
                             LearningProgressService learningProgressService,
                             TeacherService teacherService,
                             HomeworkService homeworkService) {
        this.userService = userService;
        this.storageService = storageService;
        this.courseService = courseService;
        this.learningProgressService = learningProgressService;
        this.teacherService = teacherService;
        this.homeworkService = homeworkService;
    }

    @GetMapping("/profile")
    public String showProfilePage(Model model) {
        try {
            AcademyUser academyUser = getAuthenticatedUser();
            long numberOfCompletedCourses = learningProgressService.getNumberOfCompletedCourses(academyUser.getUserId());
            long numberOfEnrolledCourses = learningProgressService.getNumberOfEnrolledCourses(academyUser.getUserId());
            File image = storageService.loadAsResource(academyUser.getImage().getImage()).getFile();
            model.addAttribute("userModel", new UserDTO());
            model.addAttribute("image", Base64.getEncoder().encodeToString(Files.readAllBytes(image.toPath())));
            model.addAttribute("user", academyUser);
            model.addAttribute("enrolledCourses", numberOfEnrolledCourses);
            model.addAttribute("completedCourses", numberOfCompletedCourses);
            return "profile";
        } catch (IOException ioe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The requested data was not found");
        }
    }

    @PostMapping("/edit")
    public String editUser(@ModelAttribute(name = "userModel") UserDTO user, HttpServletRequest request) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.updateSystemUser(username, user);
        try {
            if (!user.getUsername().trim().isEmpty()) {
                logoutUser(request);
                return "redirect:/login";
            }
        } catch (ServletException se) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        if (SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities().toString().equals("[ROLE_ADMIN]")){
            return "redirect:/admin";

        }
        return "redirect:/profile";
    }

    private void logoutUser(HttpServletRequest request) throws ServletException {
        request.logout();
        SecurityContextHolder.clearContext();
    }

    @GetMapping("/profile/enrolled")
    public String showEnrolledCourses(Model model) {
        AcademyUser user = getAuthenticatedUser();
        model.addAttribute("enrolled", learningProgressService.getEnrolledWithProgress(user));
        model.addAttribute("images", courseService.getCategoryImageSrc());
        return "enrolledCourses";
    }

    @GetMapping("/profile/completed")
    public String showCompletedCourses(Model model) {
        AcademyUser academyUser = getAuthenticatedUser();
        List<Course> completedCourses = learningProgressService.getAllCompletedCourses(academyUser.getUserId());
        Map<Course, Integer> completedWithGrades =
                homeworkService.getCompletedCoursesWithAvgGrade(completedCourses, academyUser.getUserId());
        model.addAttribute("completedWithGrades", completedWithGrades);
        model.addAttribute("images", courseService.getCategoryImageSrc());
        model.addAttribute("completedCourses", completedCourses);
        return "completedCourses";
    }

    @GetMapping("/profile/submitted")
    public String showSubmittedCourses(Model model) {
        AcademyUser academyUser = getAuthenticatedUser();
        List<CourseDTORating> submittedCoursesByAuthor = courseService
                .filteredCourses(null, academyUser.getUsername(), null)
                .stream()
                .filter(CourseDTORating::isSubmitted)
                .collect(Collectors.toList());
        model.addAttribute("images", courseService.getCategoryImageSrc());
        model.addAttribute("createdCourses", submittedCoursesByAuthor);
        return "createdCourses";
    }

    @GetMapping("/profile/in-progress")
    public String showCoursesInProgress(Model model) {
        AcademyUser academyUser = getAuthenticatedUser();
        List<Course> unsubmittedCourses = teacherService.getUnsubmittedCoursesByAuthor
                (academyUser.getUserId());
        model.addAttribute("unsubmittedCourses", unsubmittedCourses);
        model.addAttribute("images", courseService.getCategoryImageSrc());
        return "coursesInProgress";
    }

    @PostMapping("/newAvatar")
    public String editProfilePicture(@RequestParam MultipartFile file) {
        AcademyUser user = getAuthenticatedUser();
        if (userService.isFileValidImage(file)) {
            userService.updateProfilePicture(user, file);
        }
        return "redirect:/profile";
    }

    private AcademyUser getAuthenticatedUser() {
        return userService.getUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }

}