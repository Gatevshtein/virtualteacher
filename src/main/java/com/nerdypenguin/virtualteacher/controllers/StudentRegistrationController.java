package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.services.contracts.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class StudentRegistrationController {
    private StudentService studentService;

    @Autowired
    public StudentRegistrationController(StudentService studentService) {
        this.studentService = studentService;

    }

    @GetMapping("/new/student")
    public String showRegisterPage(Model model) {
        model.addAttribute("student", new UserDTO());
        return "register";
    }

    @PostMapping("new/student")
    public String registerStudent(@Valid @ModelAttribute UserDTO user, HttpServletRequest request) {
        studentService.createNewStudent(user);
        try {
            request.login(user.getUsername(), user.getPassword());
        } catch (ServletException se) {
            return "redirect:/login";
        }
        return "redirect:/";
    }
}
