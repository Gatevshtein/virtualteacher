package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.services.contracts.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class TeacherRegistrationController {
    private TeacherService teacherService;

    @Autowired
    public TeacherRegistrationController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/new/teacher")
    public String showRegisterPage(Model model) {
        model.addAttribute("teacher", new UserDTO());
        return "registerTeacher";
    }

    @PostMapping("/new/teacher")
    public String registerStudent(@Valid @ModelAttribute UserDTO user, HttpServletRequest request) {
        teacherService.createNewTeacher(user);
        try {
            request.login(user.getUsername(), user.getPassword());
        } catch (ServletException se) {
            return "redirect:/login";
        }
        return "redirect:/";
    }
}
