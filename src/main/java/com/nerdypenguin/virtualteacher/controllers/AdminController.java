package com.nerdypenguin.virtualteacher.controllers;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.services.contracts.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Controller
public class AdminController {
    private StudentService studentService;
    private TeacherService teacherService;
    private UserService userService;
    private CourseService courseService;
    private StorageService storageService;
    private AdminService adminService;

    public AdminController(StudentService studentService, TeacherService teacherService,
                           UserService userService, CourseService courseService,
                           StorageService storageService, AdminService adminService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.userService = userService;
        this.courseService = courseService;
        this.storageService = storageService;
        this.adminService = adminService;
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showAdminPage(Model model){
        try {
            SystemUser systemUser = getAuthenticatedUser();
            File image = storageService.loadAsResource(systemUser.getImage().getImage()).getFile();
            model.addAttribute("systemUserModel", new UserDTO());
            model.addAttribute("admin", systemUser);
            model.addAttribute("image", Base64.getEncoder()
                    .encodeToString(Files.readAllBytes(image.toPath())));
            return"admin";
        }catch (IOException ioe){
            throw new IllegalArgumentException();
        }
    }

    @PostMapping("/admin/newAvatar")
    public String editAdminPicture(@RequestParam MultipartFile file){
        SystemUser admin = getAuthenticatedUser();
        if(userService.isFileValidImage(file)){
        adminService.updateAdminPicture(admin, file);}
        return "redirect:/admin";
    }

    @DeleteMapping("/admin/deleteuser/{userId}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteUser(@PathVariable int userId){
        adminService.deleteAcademyUser(userId);
        return "redirect:/admin/teachers";
    }

    @DeleteMapping("/admin/deletecourse/{courseId}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteCourse(@PathVariable int courseId){
        Course courseForDelete = courseService.getCourse(courseId);
        courseService.deleteCourse(courseForDelete);
        return "redirect:/admin/courses";
    }

    @PostMapping("/admin/approve/{userId}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String approvePendingAccount(@PathVariable int userId){
        adminService.approveUserAccount(userId);
        return "redirect:/admin/pending";
    }

    @PostMapping("/admin/reject/{userId}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String rejectPendingAccount(@PathVariable int userId){
        adminService.rejectUserAccount(userId);
        return "redirect:/admin/pending";
    }

    @PostMapping("/admin/new")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String registerNewAdmin(@Valid @ModelAttribute UserDTO user){
        adminService.createNewAdmin(user);
        return "redirect:/admin";
    }

    @GetMapping("/admin/pending")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showPendingAccounts(Model model){
        List<AcademyUser> pendingAccounts = adminService.getAllPendingAccounts();
        Map<AcademyUser, String> accountsWithImages = userService.getUsersWithImages(pendingAccounts);
        model.addAttribute("pending", accountsWithImages);
        return "pending";
    }

    @GetMapping("/admin/courses")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showAllCourses(Model model){
        List<Course> allCourses = courseService.getAllCourses();
        model.addAttribute("images", courseService.getCategoryImageSrc());
        model.addAttribute("courses", allCourses);
        return "allCourses";
    }

    @GetMapping("/admin/teachers")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showAllTeachers(Model model){
        List<AcademyUser> teachers = teacherService.getAllTeachers();
        Map<AcademyUser, String> accountsWithImages = userService.getUsersWithImages(teachers);
        model.addAttribute("teachers", accountsWithImages);
        return "allTeachers";
    }

    @GetMapping("/admin/students")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String showAllStudents(Model model){
        List<AcademyUser> students = studentService.getAllStudents();
        Map<AcademyUser, String> accountsWithImages = userService.getUsersWithImages(students);
        model.addAttribute("students", accountsWithImages);
        return "allStudents";
    }

    private SystemUser getAuthenticatedUser() {
        return adminService.getSystemUserByUsername(SecurityContextHolder.getContext()
                .getAuthentication()
                .getName());
    }
}

