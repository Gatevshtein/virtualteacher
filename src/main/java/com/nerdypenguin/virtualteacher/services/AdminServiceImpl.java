package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Image;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.AdminService;
import com.nerdypenguin.virtualteacher.services.contracts.EmailService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private StorageService storageService;
    private EmailService emailService;
    private UserService userService;
    private static final String APPROVE_MESSAGE =
            "Congrats! \n\nYour teacher account in 'Nerdy Penguin' is now active! " +
                    "\n\n Cheers!";
    private static final String REJECT_MESSAGE = "Sorry! \n\nYour application for a teacher account" +
            "in 'Nerdy Penguin' was rejected.\nYou are still welcome to use the student " +
            "functionality to its fullest! \n\nHave a nice day!";

    @Autowired
    public AdminServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                            StorageService storageService,
                            EmailService emailService,
                            UserService userService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.storageService = storageService;
        this.emailService = emailService;
        this.userService = userService;
    }

    @Override
    public SystemUser getSystemUserByUsername(String username){
        SystemUser admin = userRepository.getSystemUserByUsername(username);
        Role adminRole = userRepository.getRole("ROLE_ADMIN");
        if (admin == null || (!admin.getRoles().contains(adminRole))){
            throw new IllegalArgumentException(String.format("The user with username %s is not admin", username));
        }
        return admin;
    }

    @Override
    public void updateAdminPicture(SystemUser user, MultipartFile file){
        Image image = new Image("ImageOf" + user.getUsername() + user.getUserId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        userRepository.createImage(image);
        storageService.store(file, "ImageOf" + user.getUsername() + user.getUserId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        user.setImage(image);
        userRepository.updateSystemUser(user);
    }

    @Override
    public void createNewAdmin(UserDTO userDTO) {
        try {
            userService.validateUsername(userDTO.getUsername());
            userService.validatePassword(userDTO.getPassword());
            userService.validateEmail(userDTO.getEmail());

            String encoded = passwordEncoder.encode(userDTO.getPassword().trim());
            SystemUser newAdmin = new SystemUser(userDTO.getUsername().trim(), encoded, userDTO.getEmail().trim());
            newAdmin.setAdmin(true);
            newAdmin.setImage(userRepository.getDefaultImage("ROLE_ADMIN"));
            newAdmin.getRoles().add(userRepository.getRole("ROLE_ADMIN"));
            userRepository.createSystemUser(newAdmin);

        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public List<AcademyUser> getAllPendingAccounts() {
        return userRepository.getAllPendingAccounts();
    }

    @Override
    public void approveUserAccount(int userId) {
        AcademyUser user = userRepository.getUserById(userId);
        if(user == null){
            throw new IllegalArgumentException("User doesn't exist");
        }
        userRepository.approveTeacherAccount(userId);
        emailService.sendEmail(user.getEmail(), APPROVE_MESSAGE);
    }

    @Override
    public void rejectUserAccount(int userId) {
        AcademyUser user = userRepository.getUserById(userId);
        if(user == null){
            throw new IllegalArgumentException("User doesn't exist");
        }
        userRepository.rejectTeacherAccount(user);
        emailService.sendEmail(user.getEmail(), REJECT_MESSAGE);
    }

    @Override
    public void deleteAcademyUser(int userId) {
        AcademyUser userForDelete = userRepository.getUserById(userId);
        if (userForDelete == null) {
            throw new IllegalArgumentException(String.format("User with id %d doesn't exist", userId));
        }
        userRepository.deleteAcademyUser(userId);
    }
}
