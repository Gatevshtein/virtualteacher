package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.*;
import com.nerdypenguin.virtualteacher.repositories.contracts.HomeworkRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LearningProgressRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.HomeworkService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HomeworkServiceImpl implements HomeworkService {
    private HomeworkRepository homeworkRepository;
    private UserRepository userRepository;
    private final StorageService storageService;
    private LessonRepository lessonRepository;
    private LearningProgressRepository learningProgressRepository;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepository homeworkRepository,
                               UserRepository userRepository,
                               StorageService storageService, LessonRepository lessonRepository,
                               LearningProgressRepository learningProgressRepository) {
        this.homeworkRepository = homeworkRepository;
        this.userRepository = userRepository;
        this.storageService = storageService;
        this.lessonRepository = lessonRepository;
        this.learningProgressRepository = learningProgressRepository;
    }

    @Override
    public Homework getHomeworkByID(int id) {
        return homeworkRepository.getHomeworkByID(id);
    }

    @Override
    public void uploadStudentHomework(String username, int lessonId, MultipartFile homeworkFile) {
        try {
            AcademyUser student = userRepository.getUserByUsername(username);
            Lesson lesson = lessonRepository.getLesson(lessonId);

            if (lesson == null || student == null) {
                throw new IllegalArgumentException("The lesson or the user does not exist");
            }
            if (!student.getEnrolledCourses().contains(lesson.getCourse())) {
                throw new IllegalArgumentException("You must enroll in the course before submit homework");
            }

            Homework homework = new Homework(student, lesson, username + lessonId + "." + FilenameUtils.getExtension(homeworkFile.getOriginalFilename()), 0);
            homeworkRepository.uploadHomework(homework);
            storageService.store(homeworkFile, username + lessonId + "." + FilenameUtils.getExtension(homeworkFile.getOriginalFilename()));
        } catch (RuntimeException re) {
            throw new IllegalArgumentException(re.getMessage());
        }
    }

    @Override
    public List<Homework> getHomeworksWithoutGrade(int teacherId) {
        AcademyUser user = userRepository.getUserById(teacherId);
        Role teacherRole = userRepository.getRole("ROLE_TEACHER");
        if (!user.getRoles().contains(teacherRole)) {
            throw new IllegalArgumentException("Only teachers have access to judge system");
        }
        return homeworkRepository.getHomeworksWithoutGrade(teacherId);
    }

    @Override
    public int getAVGradeOfStudent(int studentID, int courseID) {
        return learningProgressRepository.getAVGradeOfStudent(studentID, courseID);
    }

    public int getGradeOfHomework(int studentID, int lessonID) {
        if (wasHomeworkSubmitted(studentID, lessonID)) {
            return homeworkRepository.getGradeOfHomework(studentID, lessonID);
        } else return 0;
    }

    @Override
    public boolean wasHomeworkSubmitted(int studentID, int lessonID) {
        return learningProgressRepository.wasHomeworkSubmitted(studentID, lessonID);
    }

    @Override
    public void gradeHomework(int homeworkId, int grade, int teacherId) {

        Homework currentHomework = homeworkRepository.getHomeworkByID(homeworkId);
        if (currentHomework.getLesson().getCourse().getAuthor().getUserId() != teacherId) {
            throw new IllegalArgumentException("Only the author of the course can grade its homeworks");
        }

        homeworkRepository.gradeHomework(homeworkId, grade);

        if (isUserCompletedSuccessfullyTheCourse(homeworkId)) {

            Homework homework = homeworkRepository.getHomeworkByID(homeworkId);
            Lesson currentLesson = homework.getLesson();
            int studentId = homework.getStudent().getUserId();
            int courseId = currentLesson.getCourse().getId();
            learningProgressRepository.addCompletedCourse(studentId, courseId);
        }
    }

    private boolean isUserCompletedSuccessfullyTheCourse(int homeworkId) {

        Homework homework = homeworkRepository.getHomeworkByID(homeworkId);
        Lesson currentLesson = homework.getLesson();
        AcademyUser student = homework.getStudent();
        Course course = currentLesson.getCourse();
        int indexOfTheLastLesson = course.getLessons().size() - 1;
        Lesson lastLessonInTheCourse = course.getLessons().get(indexOfTheLastLesson);

        int studentAvgGradeFromCourse =
                learningProgressRepository.getAVGradeOfStudent(student.getUserId(), course.getId());

        return student.getWatchedLessons().contains(lastLessonInTheCourse)
                && studentAvgGradeFromCourse >= Course.passGrade;
    }

    @Override
    public Map<Course, Integer> getCompletedCoursesWithAvgGrade(List<Course> completedCourses, int studentId){
        Map<Course, Integer> completedWithGrades = new HashMap<>();
        completedCourses.forEach(course -> completedWithGrades.put
                (course, getAVGradeOfStudent(studentId, course.getId())));
        return completedWithGrades;
    }
}
