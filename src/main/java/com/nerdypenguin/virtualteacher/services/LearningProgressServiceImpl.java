package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.CompletedCourse;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LearningProgressRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.LearningProgressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LearningProgressServiceImpl implements LearningProgressService {
    private CourseRepository courseRepository;
    private UserRepository userRepository;
    private LearningProgressRepository learningProgressRepository;
    private LessonRepository lessonRepository;

    @Autowired
    public LearningProgressServiceImpl(CourseRepository courseRepository,
                                       UserRepository userRepository,
                                       LearningProgressRepository learningProgressRepository,
                                       LessonRepository lessonRepository) {
        this.courseRepository = courseRepository;
        this.userRepository = userRepository;
        this.learningProgressRepository = learningProgressRepository;
        this.lessonRepository = lessonRepository;
    }

    @Override
    public boolean hasUserCompletedCourse(int userID, int courseID) {
        return learningProgressRepository.hasUserCompletedCourse(userID, courseID);
    }

    @Override
    public boolean isPreviousHomeworkSubmitted(String username, int lessonID, int courseID) {

        Course course = courseRepository.getCourse(courseID);
        AcademyUser student = userRepository.getUserByUsername(username);
        Lesson currentLessonToWatch = lessonRepository.getLesson(lessonID);
        if (course == null || currentLessonToWatch == null) {
            throw new IllegalArgumentException("Not found");
        }
        int orderOfLesson = course.getLessons().indexOf(currentLessonToWatch);

        if (orderOfLesson == 0) {
            return true;
        }
        Lesson previousLesson = course.getLessons().get(orderOfLesson - 1);
        return learningProgressRepository.wasHomeworkSubmitted(student.getUserId(), previousLesson.getId());
    }

    @Override
    public void enrollCourse(int userId, int courseId) {
        try {
            AcademyUser user = userRepository.getUserById(userId);
            Course course = courseRepository.getCourse(courseId);

            if (user.getEnrolledCourses().contains(course)) {
                throw new IllegalArgumentException("You've already enrolled in this course");
            }
            learningProgressRepository.enrollCourse(userId, courseId);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void gradeCourse(int userId, int courseId, int ratingFromStudent) {
        try {
            CompletedCourse completedCourse =
                    learningProgressRepository.findCompletedCourse(userId, courseId);
            if (completedCourse == null) {
                throw new IllegalArgumentException("You didn't successfully complete this course yet!");
            }
            learningProgressRepository.gradeCourse(userId, courseId, ratingFromStudent);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public List<Course> getAllCompletedCourses(int userId) {
        return learningProgressRepository.getAllCompletedCourses(userId);
    }

    @Override
    public void addCompletedLessonToUser(AcademyUser user, int lessonID) {
        Lesson lesson = lessonRepository.getLesson(lessonID);
        if (lesson.getCourse().getAuthor().getUserId() == user.getUserId()){
            throw new IllegalArgumentException("The author of the course cannot complete its lessons");
        }
        if (!user.getEnrolledCourses().contains(lesson.getCourse())) {
            throw new IllegalArgumentException("You must first enroll for the course");
        }
        else {
            user.getWatchedLessons().add(lesson);
            userRepository.updateAcademyUser(user);
        }
    }

    @Override
    public long getNumberOfEnrolledCourses(int userId) {
        return learningProgressRepository.getNumberOfEnrolledCourses(userId);
    }

    @Override
    public long getNumberOfCompletedCourses(int userId) {
        return learningProgressRepository.getNumberOfCompletedCourses(userId);
    }

    @Override
    public long calculateUserProgressInCourse(Course course, AcademyUser user) {
        long watchedLessons = user.getWatchedLessons()
                .stream()
                .filter(lesson -> lesson.getCourse().getId() == course.getId())
                .count();
        if (watchedLessons == 0) {
            return 0;
        } else {
            long courseSize = course.getLessons().size();
            return (watchedLessons * 100) / courseSize;
        }
    }

    @Override
    public Map<Course, Long> getEnrolledWithProgress(AcademyUser user) {
        Map<Course, Long> enrolledWithProgress = new HashMap<>();
        Set<Course> enrolled = user.getEnrolledCourses();
        enrolled.forEach
                (course -> enrolledWithProgress.put(course,
                        calculateUserProgressInCourse(course, user)));
        return enrolledWithProgress;
    }

}
