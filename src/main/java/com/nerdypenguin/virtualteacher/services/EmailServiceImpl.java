package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class  EmailServiceImpl implements EmailService {

    private JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(String userEmail, String emailText) {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("virtual.teacher.app@mail.bg");
        msg.setTo(userEmail);
        msg.setSubject("Virtual Teacher Application Status");
        msg.setText(emailText);

        javaMailSender.send(msg);

    }

}
