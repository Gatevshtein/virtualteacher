package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.LessonDTO;
import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.VideoRepository;
import com.nerdypenguin.virtualteacher.services.contracts.LessonService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import static com.nerdypenguin.virtualteacher.services.utils.YoutubeURLParser.*;

import static java.lang.Integer.parseInt;

@Service
public class LessonServiceImpl implements LessonService {
    private CourseRepository courseRepository;
    private StorageService storageService;
    private LessonRepository lessonRepository;
    private VideoRepository videoRepository;

    @Autowired
    public LessonServiceImpl(CourseRepository courseRepository, StorageService storageService,
                             LessonRepository lessonRepository, VideoRepository videoRepository) {
        this.courseRepository = courseRepository;
        this.storageService = storageService;
        this.lessonRepository = lessonRepository;
        this.videoRepository = videoRepository;
    }

    @Override
    public Lesson getLesson(int id) {
        Lesson lesson = lessonRepository.getLesson(id);
        if (lesson != null) {
            return lesson;
        } else throw new IllegalArgumentException("Lesson not found");
    }

    @Override
    public void deleteLesson(AcademyUser academyUser, int lessonID) {
        Lesson lesson = lessonRepository.getLesson(lessonID);
        Course course = lesson.getCourse();
        if (course.getAuthor().getUserId() != academyUser.getUserId()) {
            throw new IllegalArgumentException("Only the course author can edit this content");
        } else if (course.isSubmitted()) {
            throw new IllegalArgumentException("The lesson cannot be deleted after the course's submission");
        }
        lessonRepository.deleteLesson(lessonID);
    }

    @Override
    public void updateLesson(AcademyUser academyUser, int lessonID, String description) {
        Lesson lesson = getLesson(lessonID);
        if (academyUser.getUserId() == lesson.getCourse().getAuthor().getUserId()) {
            lesson.setLessonDescription(description);
            lessonRepository.updateLesson(lesson);
        } else throw new IllegalArgumentException("The lesson can be edited only by its author");
    }

    @Override
    public void addLessonToCourse(LessonDTO lessonDTO,
                                  int courseID,
                                  String videoTitle,
                                  String videoURL,
                                  String homeworkFileName,
                                  MultipartFile file) {
        //todo - refactor
        Course course = courseRepository.getCourse(courseID);
        if (course == null) {
            throw new IllegalArgumentException("Not found");
        }

        checkIfVideoTitleIsFree(videoTitle);
        Video newVideo = new Video(videoTitle.trim(), convertYouTubeURL(videoURL));
        String fileName = "Homework_" + lessonDTO.getTitle()
                + courseID + "." + FilenameUtils.getExtension(file.getOriginalFilename());

        Lesson lesson = new Lesson(lessonDTO.getTitle().trim(), newVideo, course,
                lessonDTO.getLessonDescription().trim(), fileName);

        storageService.store(file, fileName);

        courseRepository.addLessonToCourse(lesson);
    }

    private void checkIfVideoTitleIsFree(String videoTitle) {
        if (!videoRepository.isVideoTitleFree(videoTitle)) {
            throw new IllegalArgumentException(
                    "Video with such title already exists, please choose a different one");
        }
    }

}
