package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.repositories.contracts.VideoRepository;
import com.nerdypenguin.virtualteacher.services.contracts.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VideoServiceImpl implements VideoService {

    private VideoRepository videoRepository;

    @Autowired
    public VideoServiceImpl(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }

    @Override
    public Video getVideo(int video_id) {
        return videoRepository.getVideo(video_id);
    }

    @Override
    public void saveVideo(Video video) {
        videoRepository.saveVideo(video);
    }

    public boolean isVideoTitleFree(String videoTitle) {
        return videoRepository.isVideoTitleFree(videoTitle);
    }
}
