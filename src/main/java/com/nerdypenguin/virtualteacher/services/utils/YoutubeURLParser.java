package com.nerdypenguin.virtualteacher.services.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface YoutubeURLParser {

    static String convertYouTubeURL(String originalUrl) {
        StringBuilder linkBuilder = new StringBuilder();
        linkBuilder.append("https://www.youtube.com/embed/");
        String pattern =
                "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D" +
                        "|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F" +
                        "|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(originalUrl);
        String youtubeId = "";
        if (matcher.find()) {
            youtubeId = matcher.group();
        }
        if (youtubeId.length() < 11) {
            throw new IllegalArgumentException("Please enter a valid Youtube URL");
        }
        linkBuilder.append(youtubeId);
        return linkBuilder.toString();
    }
}
