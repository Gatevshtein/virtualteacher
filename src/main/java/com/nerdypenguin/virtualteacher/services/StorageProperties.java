package com.nerdypenguin.virtualteacher.services;

import org.springframework.stereotype.Service;

@Service
public class StorageProperties {

    private String location = "./files";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}