package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Image;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private StorageService storageService;
    private static final Pattern USERNAME_PATTERN = Pattern.compile("^[A-Za-z0-9_-]{5,50}$");
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("[^' ']*");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$");

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder, StorageService storageService) {
        this.userRepository = userRepository;

        this.passwordEncoder = passwordEncoder;
        this.storageService = storageService;
    }

    @Override
    public AcademyUser getUserByUsername(String username) {

        AcademyUser user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format
                    ("User with username %s does not exist", username));
        }
        return user;
    }

    @Override
    public void updateSystemUser(String username, UserDTO modelWithUpdates) {
        try {
            SystemUser systemUser = userRepository.getSystemUserByUsername(username);
            if (systemUser == null){
                throw new IllegalArgumentException(String.format("User with username %s does not exist", username));
            }

            if (!modelWithUpdates.getUsername().trim().isEmpty()) {
                validateUsername(modelWithUpdates.getUsername());
                systemUser.setUsername(modelWithUpdates.getUsername().trim());
            }
            if (!modelWithUpdates.getEmail().trim().isEmpty()) {
                    validateEmail(modelWithUpdates.getEmail());
                    systemUser.setEmail(modelWithUpdates.getEmail().trim());
            }
            if (!modelWithUpdates.getPassword().trim().isEmpty()) {
                validatePassword(modelWithUpdates.getPassword());
                String encode = passwordEncoder.encode(modelWithUpdates.getPassword().trim());
                systemUser.setPassword(encode);
            }
            userRepository.updateSystemUser(systemUser);

        } catch (RuntimeException re) {
            throw new IllegalArgumentException(re.getMessage());
        }
    }

    @Override
    public void updateProfilePicture(AcademyUser user, @Valid MultipartFile file) {
        Image image = new Image("ImageOf" + user.getUsername() + user.getUserId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        long maxUploadSize = 5242880;
        if (file.getSize() > maxUploadSize) {
            throw new MaxUploadSizeExceededException(maxUploadSize);
        }
        storageService.store(file, "ImageOf" + user.getUsername() + user.getUserId() + "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        userRepository.createImage(image);
        user.setImage(image);
        userRepository.updateAcademyUser(user);
    }

    @Override
    public Map<AcademyUser, String> getUsersWithImages(List<AcademyUser> users) {
        try {
            Map<AcademyUser, String> usersWithImages = new HashMap<>();
            for (AcademyUser user : users) {
                File file = storageService.loadAsResource(user.getImage().getImage()).getFile();
                String data = DatatypeConverter.printBase64Binary(Files.readAllBytes(file.toPath()));
                String imageString = "data:image/png;base64," + data;
                usersWithImages.put(user, imageString);
            }
            return usersWithImages;

        } catch (IOException ioe) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean isFileValidImage(MultipartFile file) {
        if (file != null && file.getContentType() != null) {
            String mimeType = file.getContentType().split("/")[0];
            if (mimeType.equalsIgnoreCase("image")) {
                return true;
            } else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please upload a valid image");
        } else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Please upload a valid image");
    }

    public void validateUsername(String username){
        if (!USERNAME_PATTERN.matcher(username).matches()){
            throw new IllegalArgumentException
                    ("The username must contain only latin letters, numbers, and hyphens/underscores.");
        }
        if (userRepository.isUsernameUsed(username)){
            throw new IllegalArgumentException(String.format("Username %s is already used",
                    username));
        }
    }

    public void validatePassword(String password){
        if (!PASSWORD_PATTERN.matcher(password).matches()){
            throw new IllegalArgumentException
                    ("Password should be between 5 and 50 characters and contain no trailing spaces");
        }
    }

    public void validateEmail(String email){
        if (!EMAIL_PATTERN.matcher(email).matches()){
            throw new IllegalArgumentException
                    ("Please, enter a valid email address");
        }
        if (userRepository.isEmailUsed(email)){
            throw new IllegalArgumentException(String.format
                    ("Email %s is already used", email));
        }
    }

    public Role getRole(String role){
        return userRepository.getRole(role);
    }
}