package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.StudentService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @Autowired
    public StudentServiceImpl(UserRepository userRepository,
                              PasswordEncoder passwordEncoder,
                              UserService userService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    public List<AcademyUser> getAllStudents() {
        try {
            Role studentRole = userRepository.getRole("ROLE_STUDENT");
            return userRepository
                    .getAllAcademyUsers()
                    .stream()
                    .filter(academyUser -> academyUser.getRoles().contains(studentRole))
                    .collect(Collectors.toList());
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public void createNewStudent(UserDTO userDTO) {
        try {
            userService.validateUsername(userDTO.getUsername());
            userService.validatePassword(userDTO.getPassword());
            userService.validateEmail(userDTO.getEmail());

            String encoded = passwordEncoder.encode(userDTO.getPassword());
            AcademyUser newStudent = new AcademyUser
                    (userDTO.getUsername(), encoded, userDTO.getEmail());
            newStudent.setImage(userRepository.getDefaultImage("ROLE_STUDENT"));
            newStudent.getAuthorities().add(userRepository.getRole("ROLE_STUDENT"));
            userRepository.createSystemUser(newStudent);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

}
