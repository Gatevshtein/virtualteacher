package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.TeacherService;
import com.nerdypenguin.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private CourseRepository courseRepository;
    private UserService userService;
    @Autowired
    public TeacherServiceImpl(UserRepository userRepository,
                              PasswordEncoder passwordEncoder,
                              CourseRepository courseRepository,
                              UserService userService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.courseRepository = courseRepository;
        this.userService = userService;
    }

    @Override
    public List<AcademyUser> getAllTeachers() {
            try {
                return userRepository
                        .getAllAcademyUsers()
                        .stream()
                        .filter(academyUser -> academyUser.getRoles()
                                .stream().anyMatch(role -> role.getAuthority().equals("ROLE_TEACHER")))
                        .collect(Collectors.toList());
            }catch (RuntimeException e){
                throw new IllegalArgumentException(e.getMessage());
            }
    }

    @Override
    public void createNewTeacher(UserDTO userDTO) {
        try {
            userService.validateUsername(userDTO.getUsername());
            userService.validatePassword(userDTO.getPassword());
            userService.validateEmail(userDTO.getEmail());

            String encoded = passwordEncoder.encode(userDTO.getPassword());
            AcademyUser newUser = new AcademyUser
                    (userDTO.getUsername().trim(), encoded, userDTO.getEmail().trim());

            newUser.setImage(userRepository.getDefaultImage("ROLE_TEACHER"));
            newUser.getRoles().add(userRepository.getRole("ROLE_STUDENT"));
            userRepository.addAccountToWaitForApproval(newUser);
        }catch (RuntimeException e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public List<Course> getUnsubmittedCoursesByAuthor(int userId){
        AcademyUser teacher = userRepository.getUserById(userId);
        if (teacher == null){
            throw new IllegalArgumentException("Not found");
        }
        return courseRepository.getUnsubmittedCoursesByAuthor(teacher);
    }

}
