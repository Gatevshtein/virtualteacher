package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.LessonDTO;
import com.nerdypenguin.virtualteacher.models.Lesson;
import org.springframework.web.multipart.MultipartFile;

public interface LessonService {
    void addLessonToCourse (LessonDTO lessonDTO, int courseID, String videoTitle,
                            String videoURL, String homeworkFileName, MultipartFile file);

    Lesson getLesson(int id);

    void deleteLesson(AcademyUser academyUser, int lessonID);

    void updateLesson (AcademyUser academyUser, int lessonID, String description);
}
