package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.Video;

public interface VideoService {

    Video getVideo(int video_id);

    void saveVideo(Video video);
}
