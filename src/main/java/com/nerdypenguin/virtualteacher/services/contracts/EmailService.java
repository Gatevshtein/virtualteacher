package com.nerdypenguin.virtualteacher.services.contracts;

public interface EmailService {
    void sendEmail(String userEmail, String emailText);
}
