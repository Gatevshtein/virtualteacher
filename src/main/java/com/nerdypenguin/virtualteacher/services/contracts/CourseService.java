package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTO;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.models.Homework;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface CourseService {

     List<CourseDTORating> filteredCourses(String categoryName, String author, String title);

    int getCourseAVGRating(int courseID);

    Course getCourse (int id);

    Course getCourseByTitle(String title);

    List<Course> getAllCourses();

    void createCourse (CourseDTO courseDTO, String username);

    void deleteCourse (Course course);

    void updateCourse (AcademyUser academyUser, int courseID, String description);

    void submitCourse (int courseID, int userID);

    Map<Integer, String> getCategoryImageSrc();

    List<CourseDTORating> getAllCourses(String categoryName, String author,
                                        String title, String sortBy);

    Page<CourseDTORating> findPaginated(Pageable pageable, List<CourseDTORating> courses);

}
