package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;

import java.util.List;

public interface StudentService {
    List<AcademyUser> getAllStudents();
    void createNewStudent(UserDTO userDTO);

}
