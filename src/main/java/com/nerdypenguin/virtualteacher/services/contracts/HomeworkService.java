package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Homework;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface HomeworkService {

    Homework getHomeworkByID(int id);

    void uploadStudentHomework(String username, int lessonId, MultipartFile homeworkFile);

    List<Homework> getHomeworksWithoutGrade (int teacherId);

    int getAVGradeOfStudent (int student_id, int courseID);

    int getGradeOfHomework (int studentID, int lessonID);

    boolean wasHomeworkSubmitted(int studentID, int lessonID);

    void gradeHomework(int homeworkId, int grade, int teacherId);

    Map<Course, Integer> getCompletedCoursesWithAvgGrade(List<Course> completedCourses, int studentId);
}
