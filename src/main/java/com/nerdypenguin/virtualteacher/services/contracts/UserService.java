package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.Role;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UserService {

    AcademyUser getUserByUsername(String username);

    void updateSystemUser(String username, UserDTO userWithUpdates);

    void updateProfilePicture(AcademyUser user, MultipartFile file);

    Map<AcademyUser, String> getUsersWithImages(List<AcademyUser> users);

    boolean isFileValidImage(MultipartFile file);

    void validateUsername(String username);

    void validateEmail(String email);

    void validatePassword(String password);

    Role getRole(String role);
}
