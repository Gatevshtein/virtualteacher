package com.nerdypenguin.virtualteacher.services.contracts;


import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;

import java.util.List;

public interface TeacherService {
    List<AcademyUser> getAllTeachers();
    void createNewTeacher(UserDTO userDTO);
    List<Course> getUnsubmittedCoursesByAuthor(int userId);
}
