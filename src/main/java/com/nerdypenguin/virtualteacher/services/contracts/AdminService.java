package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.DTOs.UserDTO;
import com.nerdypenguin.virtualteacher.models.SystemUser;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AdminService {
    SystemUser getSystemUserByUsername(String username);
    void updateAdminPicture(SystemUser user, MultipartFile file);
    void createNewAdmin(UserDTO userDTO);
    void approveUserAccount(int userId);
    void rejectUserAccount(int userId);
    void deleteAcademyUser(int userId);
    List<AcademyUser> getAllPendingAccounts();

}
