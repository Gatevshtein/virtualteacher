package com.nerdypenguin.virtualteacher.services.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Course;

import java.util.List;
import java.util.Map;

public interface LearningProgressService {

    void addCompletedLessonToUser(AcademyUser user, int lessonID);

    long getNumberOfEnrolledCourses(int userId);

    long getNumberOfCompletedCourses(int userId);

    long calculateUserProgressInCourse(Course course, AcademyUser user);

    Map<Course, Long> getEnrolledWithProgress (AcademyUser user);

    boolean hasUserCompletedCourse (int userID, int courseID);

    boolean isPreviousHomeworkSubmitted(String username, int lessonID , int courseID);

    void enrollCourse(int userId, int courseId);

    void gradeCourse(int userId, int courseId, int ratingFromStudent);

    List<Course> getAllCompletedCourses(int userId);
}
