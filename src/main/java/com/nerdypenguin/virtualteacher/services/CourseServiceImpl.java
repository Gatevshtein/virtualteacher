package com.nerdypenguin.virtualteacher.services;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Category;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTO;
import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.repositories.contracts.CategoryRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;
    private CategoryRepository categoryRepository;
    private UserRepository userRepository;
    private final StorageService storageService;

    private final static String NOT_FOUND = "Course not found!";

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository,
                             CategoryRepository categoryRepository,
                             UserRepository userRepository,
                             StorageService storageService) {
        this.courseRepository = courseRepository;
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
        this.storageService = storageService;
    }

    @Override
    public Course getCourse(int id) {
        Course course = courseRepository.getCourse(id);
        if (course != null) {
            return course;
        } else
            throw new IllegalArgumentException(NOT_FOUND);
    }

    @Override
    public Course getCourseByTitle(String title) {
        Course course = courseRepository.getCourseByTitle(title);
        if (course != null) {
            return course;
        } else throw new IllegalArgumentException(NOT_FOUND);
    }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.getAllCourses();
    }

    @Override
    public int getCourseAVGRating(int courseID) {
        return courseRepository.getCourseAVGRating(courseID);
    }

    @Override
    public void createCourse(CourseDTO courseDTO, String username) {

        courseDTO.setAuthor(userRepository.getUserByUsername(username));
        Category category = categoryRepository.getCategory(courseDTO.getCategory());

        if (category == null) {
            throw new IllegalArgumentException("Please select a category");
        }

        if (isCourseTitleFree(courseDTO.getTitle().trim())) {
            Course course = new Course(courseDTO.getTitle().trim(),
                    courseDTO.getAuthor(),
                    category,
                    courseDTO.getDescription().trim());
            courseRepository.createCourse(course);
        } else throw new IllegalArgumentException(
                "Sorry, but this title is not free. Please rename the course!");
    }

    @Override
    public void deleteCourse(Course course) {
        course.setDeleted(true);
        courseRepository.deleteCourse(course);
    }

    @Override
    public void updateCourse(AcademyUser academyUser, int courseID, String description) {
        Course course = getCourse(courseID);
        if (academyUser.getUserId() == course.getAuthor().getUserId()) {
            course.setDescription(description.trim());
            courseRepository.updateCourse(course);
        } else throw new IllegalArgumentException("Only the course author can edit the content");
    }

    public void submitCourse(int courseID, int userID) {
        Course course = getCourse(courseID);
        AcademyUser academyUser = userRepository.getUserById(userID);
        if (course.getAuthor().getUserId() == academyUser.getUserId()) {
            if (course.getLessons().isEmpty()) {
                throw new IllegalArgumentException("A course with no content cannot be submitted");
            }
            course.setSubmitted(true);
            courseRepository.updateCourse(course);
        } else throw new IllegalArgumentException("Only the author of a course can submit it");
    }

    public Map<Integer, String> getCategoryImageSrc() {
        try {
            Map<Integer, String> map = new HashMap<>();
            for (Category c : categoryRepository.getAllCategories()) {
                File file = storageService.loadAsResource(c.getImage()).getFile();
                String data = DatatypeConverter.printBase64Binary(Files.readAllBytes(file.toPath()));
                String imageString = "data:image/png;base64," + data;
                map.put(c.getId(), imageString);
            }
            return map;
        } catch (IOException ex) {
            throw new RuntimeException("Sorry, we are having server issues");
        }
    }

    public List<CourseDTORating> filteredCourses(String categoryName,
                                                 String authorName,
                                                 String title) {
        Category category = null;
        AcademyUser author = null;

        if (categoryName != null && !categoryName.isEmpty()) {
            category = categoryRepository.getCategory(categoryName);
        }
        if (authorName != null && !authorName.isEmpty()) {
            author = userRepository.getUserByUsername(authorName);
            if (author == null) {
                return new ArrayList<CourseDTORating>();
            }
        }
        List<Course> filteredCourses = courseRepository.getFilteredCourses(category, author, title);

        return convertCoursesToDTO(filteredCourses);
    }

    @Override
    public List<CourseDTORating> getAllCourses(String categoryName, String author,
                                               String title, String sortBy) {
        List<CourseDTORating> courses = filteredCourses(categoryName, author, title);
        if (sortBy != null) {
            courses = getSortedList(courses, sortBy);
        }
        return courses;
    }

    @Override
    public Page<CourseDTORating> findPaginated(Pageable pageable, List<CourseDTORating> courses) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<CourseDTORating> list;

        if (courses.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, courses.size());
            list = courses.subList(startItem, toIndex);
        }

        Page<CourseDTORating> coursePage
                = new PageImpl<CourseDTORating>(list, PageRequest.of(currentPage, pageSize), courses.size());
        return coursePage;
    }


    private List<CourseDTORating> getCoursesSortedByRating(List<CourseDTORating> filteredCourses) {
        if (filteredCourses.size() == 0) {
            List<Course> allCourses = courseRepository.getFilteredCourses(null, null, null);
            filteredCourses = convertCoursesToDTO(allCourses);
        }
        return filteredCourses.stream().sorted(sortByRating).collect(Collectors.toList());
    }

    private List<CourseDTORating> getSortedList(List<CourseDTORating> list, String sortBy) {
        if (sortBy.equals("rating")) {
            list = getCoursesSortedByRating(list);
        } else if (sortBy.equals("title")) {
            Collections.sort(list);
        }
        return list;
    }

    private List<CourseDTORating> convertCoursesToDTO(List<Course> courses) {
        List<CourseDTORating> convertedCourses = new ArrayList<>();
        courses = courses.stream().filter(Course::isSubmitted).collect(Collectors.toList());
        for (Course course : courses) {
            CourseDTORating courseDTORating =
                    new CourseDTORating(course.getId(),
                            course.getTitle(),
                            course.getAuthor(),
                            course.getCategory(),
                            course.getDescription(),
                            course.isSubmitted());
            courseDTORating.setAvgRating(courseRepository.getCourseAVGRating(course.getId()));
            convertedCourses.add(courseDTORating);
        }
        return convertedCourses;
    }

    private boolean isCourseTitleFree(String title) {
        return courseRepository.getCourseByTitle(title) == null;
    }

    private Comparator<CourseDTORating> sortByRating = (item1, item2) ->
            (item1.getAvgRating() < item2.getAvgRating() ? 1 : -1);

}
