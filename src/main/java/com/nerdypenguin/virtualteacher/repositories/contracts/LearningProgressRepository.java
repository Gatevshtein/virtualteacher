package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.CompletedCourse;
import com.nerdypenguin.virtualteacher.models.Course;

import java.util.List;

public interface LearningProgressRepository {

    boolean hasUserCompletedCourse (int userID, int courseID);

    int getAVGradeOfStudent(int studentID, int courseID);

    boolean wasHomeworkSubmitted(int studentID, int lessonID);

    void enrollCourse(int userId, int courseId);

    void addCompletedCourse(int userId, int courseId);

    CompletedCourse findCompletedCourse(int userId, int courseId);

    void gradeCourse(int userId, int courseId, int ratingFromStudent);

    List<Course> getAllCompletedCourses(int userId);

    long getNumberOfCompletedCourses(int userId);

    long getNumberOfEnrolledCourses (int userId);
}
