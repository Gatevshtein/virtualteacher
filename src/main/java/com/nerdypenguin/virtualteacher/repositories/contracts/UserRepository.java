package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Image;
import com.nerdypenguin.virtualteacher.models.Role;
import com.nerdypenguin.virtualteacher.models.SystemUser;

import java.util.List;

public interface UserRepository {
    AcademyUser getUserById(int id);

    AcademyUser getUserByUsername(String username);

    AcademyUser getUserByEmail(String email);

    List<AcademyUser> getAllAcademyUsers();

    Role getRole(String roleName);

    Image getDefaultImage(String roleName);

    void addAccountToWaitForApproval(AcademyUser teacher);

    void approveTeacherAccount(int userId);

    void rejectTeacherAccount(AcademyUser user);

    void deleteAcademyUser(int userId);

    List<AcademyUser> getAllPendingAccounts();
//TODO do we need this one
    List<AcademyUser> getAllTeachers();

    void createSystemUser(SystemUser newAdmin);

    void updateAcademyUser(AcademyUser userToUpdate);

    void updateSystemUser(SystemUser userToUpdate);

    void createImage(Image image);

    boolean isUsernameUsed(String username);

    boolean isEmailUsed(String email);

    SystemUser getSystemUserByUsername(String username);
}
