package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.Video;

public interface VideoRepository {

    Video getVideo(int video_id);

    void saveVideo(Video video);

    boolean isVideoTitleFree(String videoTitle);
}
