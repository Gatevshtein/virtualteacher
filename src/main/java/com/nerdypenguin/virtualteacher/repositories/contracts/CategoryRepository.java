package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAllCategories ();

    Category getCategory (String category);

}
