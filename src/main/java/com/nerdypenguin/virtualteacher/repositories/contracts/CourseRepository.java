package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.Category;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Lesson;

import java.util.List;

public interface CourseRepository {

    Course getCourse (int id);

    Course getCourseByTitle(String title);

    List<Course> getAllCourses();

    List <Course> getFilteredCourses (Category category, AcademyUser creator, String title);

    void createCourse (Course course);

    void deleteCourse (Course course);

    void updateCourse (Course course);

    int getCourseAVGRating(int courseID);

    void addLessonToCourse (Lesson lesson);

    List<Course> getUnsubmittedCoursesByAuthor(AcademyUser author);

}
