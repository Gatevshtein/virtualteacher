package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.Homework;

import java.util.List;

public interface HomeworkRepository {

    Homework getHomeworkByID(int id);

    void uploadHomework(Homework homework);

    void gradeHomework(int homework_id, int grade);

    List<Homework> getHomeworksWithoutGrade(int courseID);



    int getGradeOfHomework(int studentID, int lessonID);
}
