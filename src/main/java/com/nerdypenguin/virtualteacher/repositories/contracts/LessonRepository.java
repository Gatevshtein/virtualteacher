package com.nerdypenguin.virtualteacher.repositories.contracts;

import com.nerdypenguin.virtualteacher.models.Lesson;

public interface LessonRepository {

    Lesson getLesson(int id);

    void deleteLesson(int lessonID);

    void updateLesson (Lesson lesson);
}
