package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.*;
import com.nerdypenguin.virtualteacher.repositories.contracts.UserRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("UserRepository")
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AcademyUser getUserById(int id) {
        try(Session session = sessionFactory.openSession()){
            return session.get(AcademyUser.class, id);
        }
    }

    @Override
    public List<AcademyUser> getAllAcademyUsers() {
        try(Session session = sessionFactory.openSession()){
            Query<AcademyUser> query = session.createQuery
                    ("SELECT AU FROM AcademyUser AU " +
                                    "WHERE AU.isAdmin = false",
                    AcademyUser.class);
            return query.list();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public AcademyUser getUserByUsername(String username) {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                    ("SELECT AU FROM AcademyUser AU " +
                            "WHERE AU.username like :username " +
                            "AND AU.isAdmin = false", AcademyUser.class)
                    .setParameter("username", username).uniqueResult();
        } catch (HibernateException he){
            throw new RuntimeException();
        }
    }


    @Override
    public AcademyUser getUserByEmail(String email){
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("SELECT AU from AcademyUser AU " +
                    "where AU.email LIKE :email", AcademyUser.class)
                    .setParameter("email", email).uniqueResult();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }

    public Role getRole(String roleName){
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                    ("SELECT r FROM Role AS r WHERE authority like :roleName", Role.class)
                    .setParameter("roleName", roleName).uniqueResult();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public Image getDefaultImage(String roleName){
        int imageId;
        switch (roleName){
            case "ROLE_STUDENT": imageId = 1; break;
            case "ROLE_TEACHER": imageId = 2; break;
            case "ROLE_ADMIN": imageId = 3; break;
            default: imageId = 1; break;
        }
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                    ("SELECT I from Image as I where imageId =:imageId", Image.class)
                    .setParameter("imageId", imageId)
                    .uniqueResult();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public void addAccountToWaitForApproval(AcademyUser teacher){
        try(Session session = sessionFactory.openSession()){
            PendingForApprovalRegistration newPendingTeacherAccount =
                    new PendingForApprovalRegistration(teacher);
            Transaction transaction = session.beginTransaction();
            session.save(teacher);
            session.save(newPendingTeacherAccount);
            transaction.commit();
        }
    }

    @Override
    public void approveTeacherAccount(int userId){

        try(Session session = sessionFactory.openSession()){
            AcademyUser user = session.get(AcademyUser.class, userId);
            Role role = session.get(Role.class, 1);
            PendingForApprovalRegistration pendingUser = session.createQuery
                    ("SELECT pfar FROM PendingForApprovalRegistration AS pfar " +
                            "WHERE teacher.id = :userId",
                            PendingForApprovalRegistration.class).setParameter("userId", user.getUserId())
                    .uniqueResult();

           // user.getRoles().add(getRole("ROLE_TEACHER"));
            user.getRoles().add(role);
            Transaction transaction = session.beginTransaction();
            session.update(user);
            session.delete(pendingUser);
            transaction.commit();

        }
        catch (Exception he) {
            System.out.println(he.getMessage());
        }
    }

    @Override
    public void rejectTeacherAccount(AcademyUser user){
        try(Session session = sessionFactory.openSession()){
            PendingForApprovalRegistration pendingUser = session.createQuery
                    ("SELECT PFA from PendingForApprovalRegistration as PFA " +
                            "WHERE PFA.teacher.userId =: userId ", PendingForApprovalRegistration.class)
                    .setParameter("userId", user.getUserId())
                    .uniqueResult();
            Transaction transaction = session.beginTransaction();
            session.delete(pendingUser);
            transaction.commit();
        }
    }

    public void deleteAcademyUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            AcademyUser userForDelete = session.get(AcademyUser.class, userId);
            userForDelete.setEnabled(false);
            List<Course> list = session.createQuery("SELECT c FROM Course as c " +
                    "WHERE c.author.id =:userId", Course.class)
                    .setParameter("userId", userId).list();
            Transaction transaction = session.beginTransaction();
            for (Course c : list) {
                for (Lesson l : c.getLessons()) {
                    l.setDeleted(true);
                    session.update(l);
                }
                c.setDeleted(true);
                session.update(c);
            }
            session.update(userForDelete);
            transaction.commit();
        } catch (HibernateException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public List<AcademyUser> getAllPendingAccounts() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("SELECT usr " +
                    "from PendingForApprovalRegistration as par " +
                    "JOIN par.teacher as usr ", AcademyUser.class).list();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public List<AcademyUser> getAllTeachers() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("SELECT AcademyUser as Au " +
                    "FROM AcademyUser " +
                    "JOIN Role as R " +
                    "join R.authority as A "+
                    "WHERE A LIKE 'ROLE_TEACHER'", AcademyUser.class).list();

        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public void createSystemUser(SystemUser newAdmin) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.save(newAdmin);
            transaction.commit();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public void updateAcademyUser(AcademyUser userToUpdate){
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.update(userToUpdate);
            transaction.commit();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }

    @Override
    public void createImage(Image image){
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.save(image);
            transaction.commit();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }

    @Override
    public boolean isUsernameUsed(String username){
        try(Session session = sessionFactory.openSession()){
            String checkForDuplicateUsername = session.createQuery("SELECT U.username " +
                    "FROM AcademyUser AS U " +
                    "WHERE U.username LIKE :username", String.class)
                    .setParameter("username", username)
                    .uniqueResult();
            if (checkForDuplicateUsername == null || checkForDuplicateUsername.isEmpty()){
                return false;
            }
            return true;
        }
    }

    @Override
    public boolean isEmailUsed(String email){
        try(Session session = sessionFactory.openSession()){
            String checkForDuplicateUsername = session.createQuery("SELECT U.email " +
                    "FROM AcademyUser AS U " +
                    "WHERE U.email LIKE :email", String.class)
                    .setParameter("email", email)
                    .uniqueResult();
            if (checkForDuplicateUsername == null || checkForDuplicateUsername.isEmpty()){
                return false;
            }
            return true;
        }
    }

    @Override
    public SystemUser getSystemUserByUsername(String username){
        try(Session session = sessionFactory.openSession()){
            return session.createQuery
                    ("SELECT SU FROM SystemUser SU " +
                            "WHERE SU.username like :username ", SystemUser.class)
                    .setParameter("username", username)
                    .uniqueResult();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }

    @Override
    public void updateSystemUser(SystemUser userToUpdate) {
        try(Session session = sessionFactory.openSession()){
            Transaction transaction = session.beginTransaction();
            session.update(userToUpdate);
            transaction.commit();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }
}
