package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.Homework;
import com.nerdypenguin.virtualteacher.repositories.contracts.HomeworkRepository;
import com.nerdypenguin.virtualteacher.services.contracts.StorageService;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HomeworkRepositoryImpl implements HomeworkRepository {

    private SessionFactory sessionFactory;
    private final StorageService storageService;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";

    @Autowired
    public HomeworkRepositoryImpl(SessionFactory sessionFactory,
                                  StorageService storageService) {
        this.sessionFactory = sessionFactory;
        this.storageService = storageService;
    }

    @Override
    public Homework getHomeworkByID(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Homework.class, id);
        }catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void uploadHomework(Homework homework) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            int studentID = homework.getStudent().getUserId();
            int lessonID = homework.getLesson().getId();

            if (hasThisHomeworkBeenSubmittedBefore(studentID, lessonID) == 0) {
                session.save(homework);
            } else {
                Homework homeworkToDelete = session.get(Homework.class, hasThisHomeworkBeenSubmittedBefore(studentID, lessonID));
                session.delete(homeworkToDelete);
                session.save(homework);
            }
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void gradeHomework(int homework_id, int grade) {
        try (Session session = sessionFactory.openSession()) {
            Homework homework = session.get(Homework.class, homework_id);
            homework.setGrade(grade);
            homework.setGraded(true);
            Transaction transaction = session.beginTransaction();
            session.update(homework);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public List<Homework> getHomeworksWithoutGrade(int teacherId) {
        try (Session session = sessionFactory.openSession()) {
            List<Homework> homeworks = session.createQuery("SELECT H FROM Homework H " +
                    "where H.lesson.course.author.userId =: teacherId " +
                    "AND H.isGraded = false ", Homework.class)
                    .setParameter("teacherId", teacherId).list();

            return homeworks;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    private int hasThisHomeworkBeenSubmittedBefore(int studentID, int lessonID) {
        try (Session session = sessionFactory.openSession()) {

            Homework homework = session.createQuery("SELECT h FROM Homework AS h WHERE " +
                    "h.student.id =:studentID AND " +
                    "h.lesson.id =:lessonID", Homework.class)
                    .setParameter("studentID", studentID)
                    .setParameter("lessonID", lessonID)
                    .uniqueResult();

            if (homework != null) {
                return homework.getHomework_id();
            }
            return 0;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    public int getGradeOfHomework(int studentID, int lessonID) {
        try (Session session = sessionFactory.openSession()) {
          int x = session.createQuery("SELECT h.grade FROM Homework AS h " +
                    "WHERE h.student.id =:studentID " +
                    "AND h.lesson.id =:lessonID", Integer.class)
                    .setParameter("studentID", studentID)
                    .setParameter("lessonID", lessonID).uniqueResult();
            return x;
        }catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }
}
