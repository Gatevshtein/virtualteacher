

package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.*;
import com.nerdypenguin.virtualteacher.repositories.contracts.CourseRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CourseRepositoryImpl implements CourseRepository {

    private SessionFactory sessionFactory;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Course getCourse(int id) {
        try (Session session = sessionFactory.openSession()) {
            Course course = session.get(Course.class, id);
            return course;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public Course getCourseByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Course course = session
                    .createQuery("SELECT c FROM Course as c WHERE c.title like :title", Course.class)
                    .setParameter("title", title).uniqueResult();
            return course;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    public int getCourseAVGRating(int courseID) {
        try (Session session = sessionFactory.openSession()) {

            Query query = session.createQuery("SELECT AVG (rating) " +
                    "FROM CompletedCourse AS cc " +
                    "WHERE cc.course.id=:courseID AND cc.rating > 0")
                    .setParameter("courseID", courseID);

            if (query.uniqueResult() == null || (double) query.uniqueResult() == 0) {
                return 0;
            }

            double avg = (double) query.uniqueResult();
            return (int) avg;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public List<Course> getAllCourses() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("SELECT c FROM Course AS c where isDeleted = false ", Course.class)
                    .getResultList();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public List<Course> getFilteredCourses(Category category, AcademyUser author, String title) {

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Course> query = builder.createQuery(Course.class);
            Root<Course> courseRoot = query.from(Course.class);

            query.select(courseRoot).where(builder.isTrue(courseRoot.get("isSubmitted").as(Boolean.class)));

            if (category != null) {
                query.select(courseRoot).where(builder.equal(courseRoot.get("category"), category));
            }

            if (author != null) {
                query.select(courseRoot).where(builder.equal(courseRoot.get("author"), author));
            }

            if (title != null && !title.isEmpty()) {
                query.select(courseRoot).where(builder.like(courseRoot.get("title"), "%" + title + "%"));
            }

            Query<Course> q = session.createQuery(query);

            List<Course> c = q.getResultList();
            return c;

        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void createCourse(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(course);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void deleteCourse(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            for (Lesson l : course.getLessons()) {
                l.setDeleted(true);
                session.update(l);
            }
            session.update(course);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void updateCourse(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(course);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void addLessonToCourse(Lesson lesson) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(lesson.getVideo());
            Video video = (Video) session.createQuery("SELECT v FROM Video AS v WHERE title =:title")
                    .setParameter("title", lesson.getVideo().getTitle()).uniqueResult();
            lesson.setVideo(video);
            session.save(lesson);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public List<Course> getUnsubmittedCoursesByAuthor(AcademyUser author) {

        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Course> query = builder.createQuery(Course.class);
            Root<Course> courseRoot = query.from(Course.class);

            query.select(courseRoot).where(builder.isFalse(courseRoot.get("isSubmitted").as(Boolean.class))
                    , (builder.equal(courseRoot.get("author"), author)));

            Query<Course> q = session.createQuery(query);

            List<Course> c = q.getResultList();
            return c;

        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

}