package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.Category;
import com.nerdypenguin.virtualteacher.repositories.contracts.CategoryRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private SessionFactory sessionFactory;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAllCategories() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM Category", Category.class)
                    .list();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public Category getCategory(String category) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("SELECT c FROM Category c WHERE category =:category", Category.class)
                    .setParameter("category", category)
                    .uniqueResult();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }
}
