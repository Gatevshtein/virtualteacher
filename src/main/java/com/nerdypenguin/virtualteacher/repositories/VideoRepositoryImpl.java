package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.repositories.contracts.VideoRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VideoRepositoryImpl implements VideoRepository {

    private SessionFactory sessionFactory;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";


    @Autowired
    public VideoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Video getVideo(int video_id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Video.class, video_id);
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void saveVideo(Video video) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(video);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public boolean isVideoTitleFree(String videoTitle) {
        try (Session session = sessionFactory.openSession()) {
            long count = (long) session.createQuery("SELECT COUNT(*) FROM Video  AS v " +
                    "WHERE v.title like :videoTitle")
                    .setParameter("videoTitle", videoTitle)
                    .uniqueResult();
            return count==0;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }
}
