package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.AcademyUser;
import com.nerdypenguin.virtualteacher.models.CompletedCourse;
import com.nerdypenguin.virtualteacher.models.Course;
import com.nerdypenguin.virtualteacher.models.Homework;
import com.nerdypenguin.virtualteacher.repositories.contracts.LearningProgressRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LearningProgressRepositoryImpl implements LearningProgressRepository {
    private SessionFactory sessionFactory;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";

    public LearningProgressRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public boolean hasUserCompletedCourse(int userID, int courseID) {
        try (Session session = sessionFactory.openSession()) {
            CompletedCourse course = (CompletedCourse) session.createQuery("SELECT cc FROM CompletedCourse AS cc " +
                    "WHERE cc.user.id =:userID " +
                    "AND cc.course.id=:courseID")
                    .setParameter("userID", userID)
                    .setParameter("courseID", courseID)
                    .uniqueResult();

            if (course == null) {
                return false;
            }
            return true;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public int getAVGradeOfStudent(int student_id, int course_id) {
        try (Session session = sessionFactory.openSession()) {

            double avgGrade = (double) session.createQuery("SELECT AVG(h.grade) FROM Homework AS h " +
                    "JOIN  h.lesson AS l " +
                    "WHERE h.student.id =:student_id " +
                    "AND h.lesson.course.id =:course_id")
                    .setParameter("student_id", student_id)
                    .setParameter("course_id", course_id).getResultList().get(0);
            return (int) avgGrade;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    public boolean wasHomeworkSubmitted(int studentID, int lessonID) {
        try (Session session = sessionFactory.openSession()) {
            Homework homework = session.createQuery("SELECT h FROM Homework AS h WHERE " +
                    "h.student.id =:studentID AND " +
                    "h.lesson.id =:lessonID", Homework.class)
                    .setParameter("studentID", studentID)
                    .setParameter("lessonID", lessonID)
                    .uniqueResult();
            if (homework == null) {
                return false;
            } else return true;
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void enrollCourse(int userId, int courseId){
        try(Session session = sessionFactory.openSession()){
            AcademyUser user = session.get(AcademyUser.class, userId);
            Course courseToEnroll = session.get(Course.class, courseId);
            user.getEnrolledCourses().add(courseToEnroll);
            Transaction transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    @Override
    public void addCompletedCourse(int userId, int courseId) {
        try(Session session = sessionFactory.openSession()){
            AcademyUser user = session.get(AcademyUser.class, userId);
            Course course = session.get(Course.class, courseId);
            user.getEnrolledCourses().remove(course);
            CompletedCourse completedCourse = new CompletedCourse(user, course, 0);

            Transaction transaction = session.beginTransaction();
            session.save(completedCourse);
            session.save(user);
            transaction.commit();

        }catch (HibernateException e){
            throw new RuntimeException();
        }
    }

    public CompletedCourse findCompletedCourse(int userId, int courseId){
        try(Session session = sessionFactory.openSession()){
            CompletedCourse cc = session.createQuery("SELECT cc FROM CompletedCourse AS cc " +
                    "WHERE cc.user.id =:userId " +
                    "AND cc.course.id =:courseId ", CompletedCourse.class)
                    .setParameter("userId", userId)
                    .setParameter("courseId", courseId).uniqueResult();
            return cc;
        }
    }

    public void gradeCourse(int userId, int courseId, int ratingFromStudent){

        try(Session session = sessionFactory.openSession()){
            CompletedCourse courseToBeGrade = findCompletedCourse(userId, courseId);
            courseToBeGrade.setRating(ratingFromStudent);
            Transaction transaction = session.beginTransaction();
            session.update(courseToBeGrade);
            transaction.commit();
        }
    }

    @Override
    public List<Course> getAllCompletedCourses(int userId) {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("SELECT C " +
                    "FROM CompletedCourse as CC " +
                    "JOIN CC.course as C " +
                    "join CC.user as AU " +
                    "WHERE AU.userId =:userId ", Course.class)
                    .setParameter("userId", userId).list();
        }
    }

    @Override
    public long getNumberOfEnrolledCourses (int userId){
        try(Session session = sessionFactory.openSession()){
            AcademyUser user = session.get(AcademyUser.class, userId);
            return user.getEnrolledCourses().size();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }

    @Override
    public long getNumberOfCompletedCourses(int userId){
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("SELECT count (CC) from CompletedCourse CC " +
                    "WHERE CC.user.id =:userId", Long.class)
                    .setParameter("userId", userId)
                    .uniqueResult();
        }catch (HibernateException he){
            throw new RuntimeException();
        }
    }


}
