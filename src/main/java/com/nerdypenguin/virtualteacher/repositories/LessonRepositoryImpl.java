package com.nerdypenguin.virtualteacher.repositories;

import com.nerdypenguin.virtualteacher.models.Lesson;
import com.nerdypenguin.virtualteacher.repositories.contracts.LessonRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LessonRepositoryImpl implements LessonRepository {
    private SessionFactory sessionFactory;
    private final static String EXCEPTION_MSG = "Sorry for the inconvenience, we are experiencing connectivity issues";


    @Autowired
    public LessonRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Lesson getLesson(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Lesson.class, id);
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    public void deleteLesson(int lessonID) {
        try (Session session = sessionFactory.openSession()) {
            Lesson lesson = session.get(Lesson.class, lessonID);
            lesson.setDeleted(true);
            Transaction transaction = session.beginTransaction();
            session.update(lesson);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }

    @Override
    public void updateLesson(Lesson lesson) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(lesson);
            transaction.commit();
        } catch (HibernateException he) {
            throw new RuntimeException(EXCEPTION_MSG);
        }
    }
}
