package com.nerdypenguin.virtualteacher.configurations;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;


    @Configuration
    @PropertySource("classpath:application.properties")
    @EnableTransactionManagement
    public class HibernateConfig {
        private String dbUrl, dbUsername, dbPassword;

        @Autowired
        public HibernateConfig(Environment env) {
            this.dbUrl = env.getProperty("database.url");
            this.dbUsername = env.getProperty("database.username");
            this.dbPassword = env.getProperty("database.password");
            System.out.println(dbUrl + " " + dbUsername + " " + dbPassword);
        }

        @Bean
        public LocalSessionFactoryBean sessionFactory() {
            LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
            sessionFactory.setDataSource(dataSource());
            sessionFactory.setPackagesToScan("com.nerdypenguin.virtualteacher.models");
            sessionFactory.setHibernateProperties(hibernateProperties());

            return sessionFactory;
        }

        @Bean
        public DataSource dataSource() {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
            dataSource.setUrl(dbUrl);
            dataSource.setUsername(dbUsername);
            dataSource.setPassword(dbPassword);

            return dataSource;
        }

        private Properties hibernateProperties() {
            Properties hibernateProperties = new Properties();
            hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");

            return hibernateProperties;
        }


        @Bean
        public TomcatServletWebServerFactory containerFactory() {
            return new TomcatServletWebServerFactory() {
                protected void customizeConnector(Connector connector) {
                    int maxSize = 50000000;
                    super.customizeConnector(connector);
                    connector.setMaxPostSize(maxSize);
                    connector.setMaxSavePostSize(maxSize);
                    if (connector.getProtocolHandler() instanceof AbstractHttp11Protocol) {

                        ((AbstractHttp11Protocol <?>) connector.getProtocolHandler()).setMaxSwallowSize(maxSize);
                        logger.info("Set MaxSwallowSize "+ maxSize);
                    }
                }
            };

        }
    }

