/*
package com.nerdypenguin.virtualteacher.restControllers;

import com.nerdypenguin.virtualteacher.models.Homework;
import com.nerdypenguin.virtualteacher.services.contracts.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/homeworks")
public class HomeworkRESTController {

    private HomeworkService homeworkService;

    @Autowired
    public HomeworkRESTController(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    @GetMapping("/avg")
    public int getAVGGrade (@RequestParam int studentID, int courseID){

        return homeworkService.getAVGradeOfStudent(studentID, courseID);

    }

    @GetMapping("/{id}")
    public Homework getHomeworkByID (@PathVariable int id){
        return homeworkService.getHomeworkByID(id);

    }

    @GetMapping("/homeworks-to-grade")
    public List <Homework> getHomeworksToGrade (@RequestParam int courseID){
        return homeworkService.  getHomeworksWithoutGrade(courseID);
    }

    @GetMapping("/grade")
    public void gradeHomework ( @RequestParam int homeworkID, @RequestParam int grade){
        homeworkService.gradeHomework(homeworkID, grade);

    }

   */
/* @PostMapping("/upload")
    public void uploadHomework ( @RequestBody Homework homework){
        homeworkService.uploadHomework(homework);
    }*//*


}
*/
