package com.nerdypenguin.virtualteacher.restControllers;

import com.nerdypenguin.virtualteacher.models.Video;
import com.nerdypenguin.virtualteacher.services.contracts.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/videos")
public class VideoRestController {

    private VideoService videoService;

    @Autowired
    public VideoRestController(VideoService videoRepository) {
        this.videoService = videoRepository;
    }

    @PostMapping("/new")
    public void uploadVideo(@RequestBody Video video){
        videoService.saveVideo(video);
    }

    @GetMapping("/{id}")
    public Video getVideo  (@PathVariable int id)  {
       return videoService.getVideo(id);
    }

}
