package com.nerdypenguin.virtualteacher.restControllers;

import com.nerdypenguin.virtualteacher.models.DTOs.CourseDTORating;
import com.nerdypenguin.virtualteacher.services.contracts.CourseService;
import com.nerdypenguin.virtualteacher.services.contracts.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/courses")
public class CourseRESTController {

    private CourseService courseService;
    private HomeworkService homeworkService;


    @Autowired
    public CourseRESTController(CourseService courseService, HomeworkService homeworkService) {
        this.courseService = courseService;
        this.homeworkService = homeworkService;
    }

    @GetMapping
    public List<CourseDTORating> getCourses(@RequestParam(required = false) String category,
                                            @RequestParam(required = false) String author,
                                            @RequestParam(required = false) String title) {
        return courseService.filteredCourses(category, author, title);
    }

    @GetMapping("/avg")
    public int getAVGResult(/*@RequestParam int studentID,*/ @RequestParam int courseID) {
        return courseService.getCourseAVGRating(courseID);
        //   return homeworkService.getAVGradeOfStudent(studentID, courseID);
    }


}
